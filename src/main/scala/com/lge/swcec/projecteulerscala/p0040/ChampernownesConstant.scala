package com.lge.swcec.projecteulerscala.p0040

import com.lge.swcec.projecteulerscala.p0000.ListUtil
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._

class ChampernownesConstant {

  def theProductOfD1D10D100D1000D10000D100000D1000000: Int = {
    val range = 1 to 1000000

    val listBuffer = new ListBuffer[Int]
    var length = 0
    breakable {
      for {
        index <- range
      } yield {
        listBuffer.append(index)
        length = index.toString.length
        if (length > 1000000) break
      }
    }

    val list = listBuffer.mkString
    val result = list(0).toString.toInt * list(9).toString.toInt *
      list(99).toString.toInt * list(999).toString.toInt *
      list(9999).toString.toInt * list(99999).toString.toInt *
      list(999999).toString.toInt

    result
  }

}