package com.lge.swcec.projecteulerscala.p0021

import com.lge.swcec.projecteulerscala.p0003.PrimeFactors

class ProperDivisors {
  
  val primeFactors = new PrimeFactors

  def getPrimeFactors(number: Int): List[Long] = {
     primeFactors.getPrimeFactors(number)
  }

  def getProperDivisors(number: Int): List[Int] = {
    List.range(1, number, 1).filter( number % _ == 0 )
  }

  def d(number: Int): Int = {
    getProperDivisors(number).sum
  }
}
