package com.lge.swcec.projecteulerscala.p0017.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0017._

class GeneratorNumberToStringTest extends FunSuite with BeforeAndAfter {

  var generatorNumberToString: GeneratorNumberToString = _
  
  before {
    generatorNumberToString = new GeneratorNumberToString
  }

  test("Problem 17: test get 1") {
    var str = generatorNumberToString.getRealStringOf(1)
    
    assert(str == "one")
  }

  test("Problem 17: test get 11") {
    var str = generatorNumberToString.getRealStringOf(11)

    assert(str == "eleven")
  }

  test("Problem 17: test get 31") {
    var str = generatorNumberToString.getRealStringOf(31)

    assert(str == "thirty one")
  }

  test("Problem 17: test get 99") {
    var str = generatorNumberToString.getRealStringOf(99)

    assert(str == "ninety nine")
  }

  test("Problem 17: test get 100") {
    var str = generatorNumberToString.getRealStringOf(100)

    assert(str == "one hundred")
  }

  test("Problem 17: test get 111") {
    var str = generatorNumberToString.getRealStringOf(111)

    assert(str == "one hundred and eleven")
  }

  test("Problem 17: test get 990") {
    var str = generatorNumberToString.getRealStringOf(990)

    assert(str == "nine hundred and ninety")
  }

  test("Problem 17: test get 999") {
    var str = generatorNumberToString.getRealStringOf(999)

    assert(str == "nine hundred and ninety nine")
  }

  test("Problem 17: test get 1000") {
    var str = generatorNumberToString.getRealStringOf(1000)

    assert(str == "one thousand")
  }

  test("Problem 17: test remove space 31") {
    var count = generatorNumberToString.getTheNumberOfLetters("thirty one")

    assert(count == 9)
  }

  test("Problem 17: test the number of 1's letter") {
    var str = generatorNumberToString.getRealStringOf(1)
    val count = generatorNumberToString.getTheNumberOfLetters(str)
    
    assert(count == 3)
  }

  test("Problem 17: test the number of 11's letter") {
    var str = generatorNumberToString.getRealStringOf(11)
    val count = generatorNumberToString.getTheNumberOfLetters(str)

    assert(count == 6)
  }

  test("Problem 17: test the number of 31's letter") {
    var str = generatorNumberToString.getRealStringOf(31)
    val count = generatorNumberToString.getTheNumberOfLetters(str)

    assert(count == 9)
  }

  test("Problem 17: test sum of 1 to 3") {
    val listString = (1 to 3).map( num => generatorNumberToString.getRealStringOf(num))
    val listNumber = listString.map(str => generatorNumberToString.getTheNumberOfLetters(str))
    val sumOfLetters = listNumber.foldLeft(0)( (a, b) => a + b )

    // one, two, three
    assert(sumOfLetters == 11)
  }

  test("Problem 17: test sum of 1 to 10") {
    val listString = (1 to 10).map( num => generatorNumberToString.getRealStringOf(num))
    val listNumber = listString.map(str => generatorNumberToString.getTheNumberOfLetters(str))
    val sumOfLetters = listNumber.foldLeft(0)( (a, b) => a + b )

    assert(sumOfLetters == 39)
  }

  test("Problem 17: test sum of 1 to 100") {
    val listString = (1 to 100).map( num => generatorNumberToString.getRealStringOf(num))
    val listNumber = listString.map(str => generatorNumberToString.getTheNumberOfLetters(str))
    val sumOfLetters = listNumber.foldLeft(0)( (a, b) => a + b )

//    println("sum of 1 to 100: " + sumOfLetters)
    
    assert(sumOfLetters == 864)
  }

  test("Problem 17: test sum of 1 to 1000") {
    val listString = (1 to 1000).map( num => generatorNumberToString.getRealStringOf(num))
    val listNumber = listString.map(str => generatorNumberToString.getTheNumberOfLetters(str))
    val sumOfLetters = listNumber.foldLeft(0)( (a, b) => a + b )

//    println("sum of 1 to 1000: " + sumOfLetters)
    
    assert(sumOfLetters == 21124)
  }

  after {
  }
}
