package com.lge.swcec.projecteulerscala.p0036.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0036.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 36: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}