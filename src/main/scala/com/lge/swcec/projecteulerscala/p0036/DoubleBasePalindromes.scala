package com.lge.swcec.projecteulerscala.p0036

import com.lge.swcec.projecteulerscala.p0004.Palindromic
import com.lge.swcec.projecteulerscala.p0000.ListUtil

class DoubleBasePalindromes extends Palindromic {

  def getTheSumOfAllNumbersLessThanOneMillionWithPalindromicBothBase10AndBase2: Int = 
    getAllNumbersLessThanOneMillionWithPalindromicBothBase10AndBase2.sum

  def getAllNumbersLessThanOneMillionWithPalindromicBothBase10AndBase2: List[Int] = {

    val range = 1 to 1000000

    val palindromes10Based = range.toList.filter(elem => {
      val string = elem.toString
      val reverse = string.reverse
      string == reverse
    })

    val palindromes10BasedAnd2Based = palindromes10Based.filter(elem => {
      val string = ListUtil.transferNumberFrom10BasedTo2Based(elem)
      val reverse = string.reverse
      string == reverse
    })
    
    palindromes10BasedAnd2Based
  }
}