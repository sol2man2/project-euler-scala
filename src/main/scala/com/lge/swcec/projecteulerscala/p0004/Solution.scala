package com.lge.swcec.projecteulerscala.p0004

class Solution {

	var palindromic: Palindromic = new Palindromic

	def doCalc {
		val largest = palindromic.getLargestPalindromicWithProductOf3DigitNumber
		
		println("===============================================================================")
		println("*************************          Problem 4          *************************")
		println("Find the largest palindrome made from the product of two 3-digit numbers.")
		println("Answer: " + largest)
		println("*************************          Problem 4          *************************")
		println("===============================================================================")
	}

}