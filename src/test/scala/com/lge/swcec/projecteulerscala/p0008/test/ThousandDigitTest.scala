package com.lge.swcec.projecteulerscala.p0008.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0008._

class ThousandDigitTest extends FunSuite with BeforeAndAfter {

  var thousandDigit: ThousandDigit = _

  before {
    thousandDigit = new ThousandDigit
  }

  test("Problem 8: test 1000 digit") {
    val max = thousandDigit.getMax
    
//    println("max: " + max)
    assert(max == 40824)
  }

  test("Find the greatest product of five consecutive digits in the 1000-digit number.") {
    val max = thousandDigit.getTheGreatestProductOfFiveConsecutiveDigitsInThe1000digitNumber

    println("max: " + max)
    
    assert(max == 40824)
  }
  
  after {
  }
  
}