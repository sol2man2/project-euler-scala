package com.lge.swcec.projecteulerscala.p0002.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0002._

class SolutionTest extends FunSuite with BeforeAndAfter {

	before {
	}

	test("Problem 2: test doCalc") {
		new Solution().doCalc
	}

	after {
	}

}