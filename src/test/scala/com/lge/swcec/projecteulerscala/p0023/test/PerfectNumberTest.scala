package com.lge.swcec.projecteulerscala.p0023.test

import scala.collection.Seq
import org.scalatest.FunSuite
import scala.collection.immutable.Map
import scala.reflect.Manifest
import scala.runtime.BoxedUnit
import java.lang.reflect.Method
import org.scalatest.BeforeAndAfter
import com.lge.swcec.projecteulerscala.p0023.PerfectNumber
import scala.collection.mutable.ListBuffer

class PerfectNumberTest extends FunSuite with BeforeAndAfter {

  var perfectNumber: PerfectNumber = _
  val upperLimitByMathematicalAnalysis = 28123
  val upperLimitForTest = 24
  
  before {
    perfectNumber = new PerfectNumber
  }

  ignore("test whether perfect number list under 10000 is List(6,28,496,8128)") {
    val list = perfectNumber.getPerfectNumber(10000)
    println("perfect number: " + list)

    assert(list == List(6, 28,496,8128))
  }

  ignore("test ListBuffer") {
    var listBufferNonAbundantNumber = ListBuffer.range(1, upperLimitByMathematicalAnalysis, 1)
    val result = perfectNumber.getAbundantNumber(upperLimitByMathematicalAnalysis)

    println("the number of abundant numbers: " + result.length)
  }

  ignore("test list drop") {
    val list = List(1,2,3,4)
    val result = list.dropWhile( _ < 3)
    println("result: " + result)
  }

  test("test whether abundant number list of things less than 28123 are List(??)") {
    val sum = perfectNumber.getSumOfNonAbundantNumbers(upperLimitByMathematicalAnalysis)

    assert(sum == 4179871)
  }

  ignore("test whether abundant number list of things less than 24 are List(??)") {
    val abundanNumberList = perfectNumber.getAbundantNumber(upperLimitForTest)

    val re = perfectNumber.getSumOfTwoNumbers(abundanNumberList, upperLimitForTest + 100)
    assert( re == List(24, 30, 32, 36,38,40) )
  }

  after {
    
  }
}
