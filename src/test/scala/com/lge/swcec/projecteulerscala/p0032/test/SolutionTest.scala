package com.lge.swcec.projecteulerscala.p0032.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0032.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 32: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}