package com.lge.swcec.projecteulerscala.p0015.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0015._

class SquareRouteTest extends FunSuite with BeforeAndAfter {

  var squareRoute: SquareRoute = _
  
  before {
    squareRoute = new SquareRoute
  }

  ignore("Problem 15: test get the number of route in 1x1 ") {
    val count = squareRoute.count(1, 1)
    println("route count: " + count)
    assert(count == 2)
  }

  ignore("Problem 15: test get the number of route in 2x2 ") {
    val count = squareRoute.count(2, 2)
    println("route count: " + count)
    assert(count == 6)
  }

  ignore("Problem 15: test get the number of route in 20x20 ") {
    val count = squareRoute.count(20, 20)
    println("route count: " + count)
    assert(count == BigInt("137846528820"))
  }

  test("Problem 15: test get the number of route in 20x20 with algorithm") {
    val count = squareRoute.countWithAlgorithm(20, 20)
    println("route count: " + count)
    assert(count == BigInt("137846528820"))
  }

  after {
  }
}
