package com.lge.swcec.projecteulerscala.p0012

class Combination {

  val factorial = new Factorial

  def getSumOfNCombination(count: Int): Long = {
    val l = (0 to count).toList
    val list = l.map(
      elem => {
        getNCombinationR(count, elem)
      }
    )
    list.foldLeft[Long](0)(_+_)
  }

  def getNCombinationR(papular: Long, select: Long): Long = {
    (
      factorial.getNFactorialWithTailRecursion(papular) 
      / factorial.getNFactorialWithTailRecursion(papular - select) 
      / factorial.getNFactorialWithTailRecursion(select)
    )
  }
}
