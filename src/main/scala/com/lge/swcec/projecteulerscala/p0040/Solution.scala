package com.lge.swcec.projecteulerscala.p0040

class Solution {

  val champernownesConstant = new ChampernownesConstant

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 40         *************************")
    println("If dn represents the nth digit of the fractional part, find the value of the following expression.")

    val theProductOfD1D10D100D1000D10000D100000D1000000 = champernownesConstant.theProductOfD1D10D100D1000D10000D100000D1000000

    println("Answer: " + theProductOfD1D10D100D1000D10000D100000D1000000)
    println("*************************          Problem 40         *************************")
    println("===============================================================================")
  }

}