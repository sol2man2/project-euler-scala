package com.lge.swcec.projecteulerscala.p0029

import java.util.HashMap
import scala.collection.mutable.HashSet

class DistinctPowers(a: Int, b: Int) {

  def getTheDistinctPowersCases(): Int = {

    val set = new HashSet[Double]
    var index = 0

    for( idxA <- (2 to a); idxB <- (2 to b) ) {
      val exponential = Math.pow(idxA, idxB)
      set.contains(exponential) match {
        case false => set += exponential
        case true =>
      }
    }

    set.size
  }
}
