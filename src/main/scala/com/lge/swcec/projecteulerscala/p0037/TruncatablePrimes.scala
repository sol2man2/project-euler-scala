package com.lge.swcec.projecteulerscala.p0037

import com.lge.swcec.projecteulerscala.p0000.ListUtil
import scala.collection.mutable.ListBuffer
import com.lge.swcec.projecteulerscala.p0000.Prime
import scala.util.control.Breaks._

class TruncatablePrimes {

  def getTheSumOf11PrimesBothLeftToRightAndRightToLeft: Int = 
    get11PrimesBothLeftToRightAndRightToLeft.sum

  def get11PrimesBothLeftToRightAndRightToLeft: List[Int] = {
    val prime = new Prime
    val primes = prime.makePrimesThruSieveOfEratosThenes(1000000).drop(4)

    var sum = 0
    var count = 0
    val listBuffer = new ListBuffer[Int]

    println("prime length: " + primes.length)

    val list = breakable {
      for {
        idx <- 0 to primes.length
      } yield {
        val number = primes(idx)
        val string = number.toString

        val arePrime = for {
          idy <- 1 to string.length - 1
        } yield {
//          if (prime.isPrime(string.slice(0, idy).toInt).get
//          && prime.isPrime(string.slice(idy, string.length).toInt).get) true
          if (prime.isPrime(string.slice(0, idy).toInt)
            && prime.isPrime(string.slice(idy, string.length).toInt)) true
          else false
        }

        if (arePrime.forall(_ == true)) {
          listBuffer.append(number)
          count = count + 1
        }

        if (count == 11) break
      }
    }
    
    listBuffer.toList
  }

}