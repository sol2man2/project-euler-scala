package com.lge.swcec.projecteulerscala.p0000.test

import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter

import com.lge.swcec.projecteulerscala.p0000._

class PrimeTest extends FunSuite with BeforeAndAfter {

  val EXAMPLE_INPUT = 13195
  val PROBLEM_INPUT = 600851475143L

  var prime: Prime = _

  before {
    prime = new Prime
  }

  test("Problem 3: test get prime factors of 600851475143L") {
    val number = 1000000
    prime.makePrimesThruSieveOfEratosThenes(number)

    val result = prime.length
    println("result: " + result)
  }
  
  test("test turn out whether number is prime or not") {
    assert(prime.isPrime(3) == true)
    assert(prime.isPrime(5) == true)
    assert(prime.isPrime(7) == true)
  }
  
  test("test turn out whether 9 is prime or not") {
    assert(prime.isPrime(9) == false)
  }

  after {
  }
}