package com.lge.swcec.projecteulerscala.p0011

class NByNMatrix(row: Int, col: Int) {

  var array = Array.ofDim[Int](row, col)

  def setData(arr: Array[Int], r: Int) {
    array(r) = arr
  }

  def getProductMax(size: Int): Int = {
    var max: Int = 0

    for(r <- 0 until array.size) {
      for(c <- 0 until array(r).size) {
        def getProductThruVertical = {
          if( c < array(r).size - 4 ) {
            val product = array(r)(c) * array(r)(c+1) * array(r)(c+2) * array(r)(c+3)
            if( max < product) max = product
          }
        }
        def getProductThruHorizontal = {
          if( r < array.size - 4 ) {
            val product = array(r)(c) * array(r+1)(c) * array(r+2)(c) * array(r+3)(c)
            if( max < product) max = product
          }
        }
        def getProductThruDiagonal = {
          if( r < array.size - 4 && c < array(r).size - 4) {
            var product = array(r)(c) * array(r+1)(c+1) * array(r+2)(c+2) * array(r+3)(c+3)
            if( max < product) max = product

            product = array(r+3)(c) * array(r+2)(c+1) * array(r+1)(c+2) * array(r)(c+3)
            if( max < product) max = product
          }
        }
        getProductThruVertical
        getProductThruHorizontal
        getProductThruDiagonal
      }
    }
    max
  }

  def display {
    for(r <- 0 until array.size) {
      for(c <- 0 until array(r).size) {
        print(array(r)(c) + ", ")
      }
      println()
    }
  }

//  def getMax: BigInt = {
//    
//  }  
}
