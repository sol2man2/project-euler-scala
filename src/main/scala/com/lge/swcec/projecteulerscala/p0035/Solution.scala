package com.lge.swcec.projecteulerscala.p0035

class Solution {

  val circularPrimes = new CircularPrimes

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 35         *************************")
    println("How many circular primes are there below one million?")

    val theNumberOfCircularPrimesBelowOneMillion = circularPrimes.getTheNumberOfCircularPrimesBelowOneMillion

    println("Answer: " + theNumberOfCircularPrimesBelowOneMillion)
    println("*************************          Problem 35         *************************")
    println("===============================================================================")
  }

}