package com.lge.swcec.projecteulerscala.p0041.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0041.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 41: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}