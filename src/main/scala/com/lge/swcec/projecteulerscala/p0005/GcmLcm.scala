package com.lge.swcec.projecteulerscala.p0005

class GcmLcm {

  def getGcm(first: Long, second: Long): Long = {
    first % second match {
      case 0 => second
      case remainder: Long => getGcm(second, remainder)
    }
  }

  def getLcm(first: Long, second: Long): Long = {
    first * second / getGcm(first, second)
  }

}