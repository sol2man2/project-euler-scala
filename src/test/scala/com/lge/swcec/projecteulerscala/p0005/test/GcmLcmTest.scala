package com.lge.swcec.projecteulerscala.p0005.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0005._

class GcmLcmTest extends FunSuite with BeforeAndAfter {

	var gcmLcm: GcmLcm = _
  
	before {
		gcmLcm = new GcmLcm
	}

	test("Problem 5: test gcm") {
		var result = gcmLcm.getGcm(6, 9)
		assert(result == 3)
		
		result = gcmLcm.getGcm(7, 11)
		assert(result == 1)

		result = gcmLcm.getGcm(15, 30)
		assert(result == 15)
		
		result = gcmLcm.getGcm(8, 92)
		assert(result == 4)
		
		result = gcmLcm.getGcm(64, 38)
		assert(result == 2)
	}

	after {
	}

}