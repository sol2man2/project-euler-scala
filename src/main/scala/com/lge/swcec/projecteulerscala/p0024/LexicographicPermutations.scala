package com.lge.swcec.projecteulerscala.p0024

import scala.collection.mutable.ListBuffer
import com.lge.swcec.projecteulerscala.p0012._

class LexicographicPermutations(seq: String) {

  val factorial = new Factorial
  val lexicographicList = {
    val lb = seq.to[ListBuffer]
    lb.sorted
  }

  val theNumberOfCasesInNDigitFixed = {
    var lb = new ListBuffer[Long]
    
    for( index <- List.range(lexicographicList.length - 1, 0, -1) ) {
      lb += factorial.getNFactorial(index)
    }
//    lb += 0
    lb.to[List]
  }

  def getMegathLexicographicPermutations(): Int = {
    0
  }

  def getTheNumberOfCasesInFixed(number: Int): Long = {
    theNumberOfCasesInNDigitFixed(number)
  }

  def getCountListOfEachDigit(number: Long): List[Int] = {
    var prevNumber = number - 1
    var lb = new ListBuffer[Int]
    for( digit <- 0 until lexicographicList.length - 1 ){
      val dividor = prevNumber / theNumberOfCasesInNDigitFixed(digit)
      val remain = prevNumber % theNumberOfCasesInNDigitFixed(digit)
      prevNumber = remain

      lb += dividor.toInt
    }
    lb += 0
    lb.to[List]
  }

  def getNthLexicographicPermutation(nth: Long): List[Char] = {

    val countList = getCountListOfEachDigit(nth)
    val sourceList = lexicographicList.to[List]
    var sourceListBuffer = sourceList.to[ListBuffer]
    var destinationListBuffer = new ListBuffer[Char]

    for( index <- 0 until sourceListBuffer.length ) {
      destinationListBuffer += sourceListBuffer.remove(countList(index))
    }

    destinationListBuffer.toList
    
//    "0123456789"
  }
  
}
