package com.lge.swcec.projecteulerscala

import com.lge.swcec.projecteulerscala.p0029._

object AppMain {

	def main(args: Array[String]): Unit = {
		println("===============================================================================")
		println("*************************     Project Euler Scala     *************************")
		new Solution().doCalc

		println("*************************     Project Euler Scala     *************************")
		println("===============================================================================")
	}

}
