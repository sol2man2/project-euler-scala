package com.lge.swcec.projecteulerscala.p0032

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

class PandigitalProducts {

  def getSumOfNinePandigitalProducts = getCasesOfNinePandigitalProducts.sum

  def getCasesOfNinePandigitalProducts = {
    getCasesOfPandigitalProducts(List(1,2,3,4,5,6,7,8,9))
  }

  def getCasesOfPandigitalProducts(digits: List[Int]) = {
    def makePandigitalProducts(multiplicandSet: List[Int]) = {
      val multiplicandDifferenceSet = digits.diff(multiplicandSet)
      val multiplierSets = getSubset(multiplicandDifferenceSet)
      val suspectMultiplierSets = multiplierSets.filter(multiplierSet => {
        val multiplierDifferenceSet = multiplicandDifferenceSet.diff(multiplierSet)
        val minimalProductLength = multiplicandSet.length + multiplierSet.length

        minimalProductLength != digits.length &&
          (minimalProductLength - 1 == digits.length - minimalProductLength
            || minimalProductLength == digits.length - minimalProductLength)
      })

      val lists = for {
        mul1 <- getPermutation(multiplicandSet)
        susp <- suspectMultiplierSets
        mul2 <- getPermutation(susp)
        prod <- getPermutation(digits.diff(mul1).diff(mul2))
        if (transferListtoInt(mul1) * transferListtoInt(mul2) == transferListtoInt(prod))
      } yield {
        transferListtoInt(prod)
      }

      lists
    }

    val suspectSubsets = getSuspectSubsets(digits)
    val result = suspectSubsets.map(makePandigitalProducts).flatten
    result.groupBy(elem => elem).keySet
  }

  def getSubset(factors: List[Int]): List[List[Int]] = {
    def makeSubset(subsets: List[List[Int]], list: List[Int]): List[List[Int]] = {
      list match {
        case head :: tail => {
          makeSubset(subsets ::: subsets.map(head :: _) ::: List(List(head)), tail)
        }
        case Nil => subsets
      }
    }
    makeSubset(Nil, factors)
  }

  def getSuspectSubsets(digits: List[Int]): List[List[Int]] = {
    getSubset(digits).filter(subset => {
      val length = digits.length
      val validDigitLength = length % 2 match {
        case 1 => length / 2 + 1
        case 0 => length / 2
      }
      validDigitLength > subset.length
    })
  }

  def getPermutation(list: List[Int]): List[List[Int]] = {
    def makePermutation(permutations: List[ListBuffer[Int]], list: List[Int]): List[List[Int]] = {
      list match {
        case head :: tail => {
          val permutationsList = permutations.flatMap(listBuffer => {
            (for (idx <- 0 to listBuffer.length) yield {
              val listBufferclone = listBuffer.clone
              listBufferclone.insert(idx, head)
              listBufferclone
            }).toList
          })
          makePermutation(permutationsList, tail)
        }
        case Nil => permutations.map(lb => lb.toList)
      }
    }
    makePermutation(List(ListBuffer(list.head)), list.tail)
  }

  def transferListtoInt(list: List[Int]) = {
    val reverse = list.reverse

    @tailrec
    def makeInt(list: List[Int], sum: Int, digit: Int): Int = {
      list match {
        case head :: tail =>
          makeInt(tail, sum + head * Math.pow(10, digit).toInt, digit + 1)
        case Nil => sum
      }
    }
    makeInt(reverse, 0, 0)
  }
}
