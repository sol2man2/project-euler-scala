package com.lge.swcec.projecteulerscala.p0003

class Solution {

	val PROBLEM_INPUT = 600851475143L

	def doCalc {
		var primeFactors = new PrimeFactors
		val list = primeFactors.getPrimeFactors(PROBLEM_INPUT)

		println("===============================================================================")
		println("*************************          Problem 3          *************************")
		println("What is the largest prime factor of the number 600851475143 ?")
		println("Answer: " + list.last)
		println("*************************          Problem 3          *************************")
		println("===============================================================================")
	}
}
