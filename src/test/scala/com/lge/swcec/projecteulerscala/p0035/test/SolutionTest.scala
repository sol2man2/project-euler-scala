package com.lge.swcec.projecteulerscala.p0035.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0035.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 35: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}