package com.lge.swcec.projecteulerscala.p0014.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0014._

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
  }

  test("Problem 14: test doCalc") {
    new Solution().doCalc
  }

  after {
  }
}