package com.lge.swcec.projecteulerscala.p0030.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0030.DigitFifthPowers

class DigitFifthPowersTest extends FunSuite with BeforeAndAfter {

  var digitFifthPowers: DigitFifthPowers = null

  before {
    digitFifthPowers = new DigitFifthPowers
  }

  test("sum of digit fifth powers is 443839") {
    val result = digitFifthPowers.getDigitFifthPowersList

    println("FifthPowers: " + result)
    
    assert( 443839 == result )
  }

  test("sum of digit fourth powers is 19316") {
    val result = digitFifthPowers.getDigitFourthPowersList

    assert( 19316 == result )
  }
  
  test("transfer \"3\" to 3") {
    val result = digitFifthPowers.getDigitListFromNumeric(3)

    assert( List(3) == result )
  }
  
  test("transfer \"25\" to 3") {
    val result = digitFifthPowers.getDigitListFromNumeric(25)

    assert( List(2, 5) == result )
  }
  
  after {
    
  }

}