package com.lge.swcec.projecteulerscala.p0040.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import scala.util.control.Breaks._
import scala.collection.mutable.ListBuffer
import scala.tools.nsc.transform.Flatten
import com.lge.swcec.projecteulerscala.p0040.ChampernownesConstant

class ChampernownesConstantTest extends FunSuite with BeforeAndAfter {

  var champernownesConstant: ChampernownesConstant = _

  before {
    champernownesConstant = new ChampernownesConstant
  }

  test("find the value of the following expression is ???") {
    val result = champernownesConstant.theProductOfD1D10D100D1000D10000D100000D1000000

    println("result: " + result)
    assert(result == 210)
  }

  after {

  }
}
