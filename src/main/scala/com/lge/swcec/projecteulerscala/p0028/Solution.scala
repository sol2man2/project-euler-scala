package com.lge.swcec.projecteulerscala.p0028

class Solution {

  val numberSpiral = new NumberSpiral(1001)

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 28         *************************")
    println("What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?")

    val theSumOfDiagonalsIn1001by1001 = numberSpiral.getTheSumOfDiagonalsInNumberSpiral

    println("Answer: " + theSumOfDiagonalsIn1001by1001)
    println("*************************          Problem 28         *************************")
    println("===============================================================================")
  }

}