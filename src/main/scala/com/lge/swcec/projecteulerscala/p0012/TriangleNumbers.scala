package com.lge.swcec.projecteulerscala.p0012

class TriangleNumbers {

  var list: List[Int] = Nil

  def getTriangleNumbers = list

  def next: Int = {
    list match {
      case Nil => list = List(1)
      case _ => list = list ::: List((list.size + 1) + list.last)
    }
    list.last
  }

  def getNthTriangleNumbers(nth: Int): Int = {
    val size = list.size
    val gap = nth - size
    val l = for(index <- 0 until gap) next
    
    list.last
  }

  def getTriangleNumbers(start: Int, finish: Int): List[Int] = {
    val seq = (1 to finish).toList
    val l = for(index <- 0 until finish) yield(seq.filter( elem => elem <= index + 1).sum)

    l.toList
  }
}