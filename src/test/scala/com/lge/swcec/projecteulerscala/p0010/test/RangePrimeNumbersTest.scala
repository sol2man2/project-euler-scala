package com.lge.swcec.projecteulerscala.p0010.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0010._
import com.lge.swcec.projecteulerscala.p0000.Prime

class RangePrimeNumbersTest extends FunSuite with BeforeAndAfter {

  var rangePrimeNumbers: RangePrimeNumbers = _
  before {
    rangePrimeNumbers = new RangePrimeNumbers
  }

  test("Find the sum of all the primes below two million.") {
    val sum = rangePrimeNumbers.getTheSumSfAllThePrimesBelowTwoMillion 
    println("sum: " + sum)
    assert(sum == 142913828922L)
  }

  after {
  }
}