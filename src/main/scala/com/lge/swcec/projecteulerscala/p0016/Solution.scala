package com.lge.swcec.projecteulerscala.p0016

class Solution {

  val sumOfDigitsOfTwoPowers = new SumOfDigitsOfTwoPowers

  def doCalc {
  val sum = sumOfDigitsOfTwoPowers.getSumOfDigitsOfTwoPowers(1000)

    println("===============================================================================")
    println("*************************          Problem 16         *************************")
    println("What is the sum of the digits of the number 2^1000?")
    println("Answer: " + sum)
    println("*************************          Problem 16         *************************")
    println("===============================================================================")
  }
}