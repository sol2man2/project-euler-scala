package com.lge.swcec.projecteulerscala.p0035.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0000.ListUtil
import com.lge.swcec.projecteulerscala.p0035.CircularPrimes
import com.lge.swcec.projecteulerscala.p0000.Prime

class CircularPrimesTest extends FunSuite with BeforeAndAfter {

  var circularPrimes: CircularPrimes = _

  before {
    circularPrimes = new CircularPrimes
  }

  ignore("study Primes of 10") {
    val prime = new Prime
    val list = prime.makePrimesThruSieveOfEratosThenes(10)
    
    println("list: " + list)
    list.foreach(println)
 
    assert(true)
  }

  test("study Primes of 1000000") {
    val prime = new Prime
//    val list = prime.makePrimesThruSieveOfEratosThenes(1000000)
    val list = prime.makePrimesThruSieveOfEratosThenes(10000000)

    println("size: " + list.length)
    println("last: " + list.last)

    assert(true)
  }

  ignore("study array") {
    val arrays = new Array[Int](1000000) 
    println("size: " + arrays.length)
    arrays(999998) = 0
    arrays(999999) = 10
    println(":" + arrays(999999))
  }

  ignore("study take & drop of list") {
    val list = List(1,9,7)
    for{ idx <- 0 to list.length - 1} {
      val suspect = list.drop(idx) ::: list.take(idx) 
      println(": " + suspect)
    }
  }

  ignore("the number of circular prime below 100 is ??") {
    val length = circularPrimes.getTheNumberOfCircularPrimesBelowSpecificNumber(100)

    assert(length == 13)
  }

  ignore("the number of circular prime below one million is ??") {
    val length = circularPrimes.getTheNumberOfCircularPrimesBelowOneMillion
    
    assert(length == 55)
  }

  after {

  }
}

