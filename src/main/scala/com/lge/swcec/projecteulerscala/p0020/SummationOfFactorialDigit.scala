package com.lge.swcec.projecteulerscala.p0020

class SummationOfFactorialDigit {

  def getSumOfFactorialDigit(number: Long) = {
    val factorial = new FactorialOnBigInt
    val numberFactorial = factorial.getNFactorial(number)
    val numberFactorialString = numberFactorial.toString

    numberFactorialString.map(ch => ch.toString.toInt).foldLeft(0)((a,b) => a+b)
  }

}