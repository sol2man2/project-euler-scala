package com.lge.swcec.projecteulerscala.p0006

class SumAndSquare {

  def getDeltaEachOther(begin: Int, end: Int): Int = {
    val range = begin to end
    val squareAfterSum = getSquareOfSum(range)
    val sumAfterSquare = getSumOfSquare(range)
    squareAfterSum - sumAfterSquare
  }

  def getSquareOfSum(start: Int, finish: Int): Int = {
    getSquareOfSum(start to finish)
  }

  def getSumOfSquare(start: Int, finish: Int): Int = {
    getSumOfSquare(start to finish)
  }

  def getSquareOfSum(range: Range): Int = {
    val list = range.toList
    val sum = list.foldLeft(0)(_ + _)
    sum * sum
  }

  def getSumOfSquare(range: Range): Int = {
    val list = range.toList
    val l = list.map(elem => elem * elem)
    l.foldLeft(0)(_ + _)
  }
}