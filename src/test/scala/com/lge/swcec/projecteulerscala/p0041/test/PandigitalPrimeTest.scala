package com.lge.swcec.projecteulerscala.p0041.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import scala.util.control.Breaks._
import scala.collection.mutable.ListBuffer
import scala.tools.nsc.transform.Flatten
import com.lge.swcec.projecteulerscala.p0041.PandigitalPrime
import com.lge.swcec.projecteulerscala.p0000.Prime

class PandigitalPrimeTest extends FunSuite with BeforeAndAfter {

  var pandigitalPrime: PandigitalPrime = _

  before {
    pandigitalPrime = new PandigitalPrime
  }

  ignore("the largest n-digit pandigital prime that exists is ???") {
    val result = pandigitalPrime.getTheLargestNdigitPandigitalPrimeThatExists

    println("result: " + result)
    assert(result == 210)
  }

  ignore("") {
    println("sqrt: " + math.sqrt(1000000000))
  }
  
  test("!") {
    val range = (0 to 1000000000)
    val prime = new Prime
//    val primes = prime.makePrimesThruSieveOfEratosThenes(1000000000)
    val primes = prime.makePrimesThruSieveOfEratosThenes(100000000)

    val map = Map(1 -> "1", 2 -> "12", 3 -> "123", 4 -> "1234", 5 -> "12345",
      6 -> "123456", 7 -> "1234567", 8 -> "12345678", 9 -> "123456789")

    var largest = 0
    
    println("primes length: " + primes.length)

//    breakable {
//      for {
//        prime <- primes.reverse
//      } yield {
//        val primeString = prime.toString
//        val length = primeString.length
//        val diffSet = primeString.diff(map(length))
//        if (diffSet == "") {
//          largest = prime
//          break
//        }
//      }
//    }

    println("largest: " + largest)

    assert(false)
  }

  after {

  }
}
