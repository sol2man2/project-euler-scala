package com.lge.swcec.projecteulerscala.p0012.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0003._
import com.lge.swcec.projecteulerscala.p0012._
import scala.collection.mutable.HashMap

class CombinationTest extends FunSuite with BeforeAndAfter {

  var combination: Combination = _
  
  before {
    combination = new Combination
  }

  test("Problem 12: test get sigma 1Cr") {
    val sum = combination.getSumOfNCombination(1)

    assert(sum == 2)
  }

  test("Problem 12: test get sigma 2Cr") {
    val sum = combination.getSumOfNCombination(2)

    assert(sum == 4)
  }

  test("Problem 12: test get sigma 3Cr") {
    val sum = combination.getSumOfNCombination(3)

    assert(sum == 8)
  }

  test("Problem 12: test get sigma 4Cr") {
    val sum = combination.getSumOfNCombination(4)

    assert(sum == 16)
  }

  test("Problem 12: test get sigma 5Cr") {
    val sum = combination.getSumOfNCombination(5)

    assert(sum == 32)
  }

  after {
  }
}