package com.lge.swcec.projecteulerscala.p0033

class Solution {

  val digitCancelingFractions = new DigitCancelingFractions

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 33         *************************")
    println("If the product of these four fractions is given in its lowest common terms, find the value of the denominator.")

    val theDenominatorOfFourFractionsProduct = digitCancelingFractions.getDenominatorOfFourFractionsProduct

    println("Answer: " + theDenominatorOfFourFractionsProduct)
    println("*************************          Problem 33         *************************")
    println("===============================================================================")
  }

}