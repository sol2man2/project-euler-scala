package com.lge.swcec.projecteulerscala.p0015

class Solution {

  val squareRoute = new SquareRoute

  def doCalc {
    val count = squareRoute.countWithAlgorithm(20, 20)

    println("===============================================================================")
    println("*************************          Problem 15         *************************")
    println("How many routes are there through a 2020 grid?")
    println("Answer: " + count)
    println("*************************          Problem 15         *************************")
    println("===============================================================================")
  }
}