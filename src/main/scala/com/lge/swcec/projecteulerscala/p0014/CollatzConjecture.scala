package com.lge.swcec.projecteulerscala.p0014

import scala.collection.mutable.HashMap

class CollatzConjecture {

  var map = new HashMap[Long, Long]
  map.put(1, 1)

  def next(number: Long): Long = {
    number match {
      case n if(n == 1) => 1
      case n if(n % 2 == 0) => n / 2
      case n if(n % 2 == 1) => 3 * n + 1
    }
  }

  def sequenceCountOf(number: Long): Long = {

    val list = calcSequenceCountOf(number)
    registerCollatzToMap(list)
  }
  
  def registerCollatzToMap(list: List[Long]): Long = {
    list match {
      case Nil => {
        0
      }
      case head :: Nil => {
        val count = map.get(next(head)).get + 1
        map.put(head, count)
        count
      }
      case head :: tails => {
        val count = registerCollatzToMap(tails) + 1
        map.put(head, count)
        count
      }
    }
  }

  def collatzHavingLongestSequenceWithin(upperLimit: Int): Long = {
    collatzSequenceWithin(upperLimit)

    var max: Long = 0
    var specialKey: Long = 0
    val list: List[Long] = map.values.toList

    map.keysIterator.foreach(
      elem => {
        if(map.get(elem).get > max) {
          specialKey = elem
          max = map.get(elem).get
        }
      }
    )
    specialKey
  }
  
  def collatzSequenceWithin(upperLimit: Int) {
    (2 to upperLimit).toList.foreach( 
      elem => {
        sequenceCountOf(elem)
      } 
    )
  }
  
  def calcSequenceCountOf(number: Long): List[Long] = {
    map.getOrElse(number, 0) match {
      case num if(num == 0) => {
        number :: calcSequenceCountOf(next(number))
      }
      case num => Nil
    }
  }
}
