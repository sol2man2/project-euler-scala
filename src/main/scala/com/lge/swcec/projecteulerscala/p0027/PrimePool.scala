package com.lge.swcec.projecteulerscala.p0027

import scala.collection.mutable.HashSet
import com.lge.swcec.projecteulerscala.p0000.Prime

class PrimePool {
  val primes: HashSet[Long] = new HashSet
  val prime: Prime =  new Prime

  def isPrime(number: Long): Boolean = {
    primes.contains(number) match {
      case true => true
      case false => {
        prime.isPrime1(number) match {
          case true => {
            primes.add(number)
            true
          }
          case false => false
        }
      }
    }
  }  
}