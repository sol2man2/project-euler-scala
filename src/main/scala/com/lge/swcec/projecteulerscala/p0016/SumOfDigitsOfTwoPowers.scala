package com.lge.swcec.projecteulerscala.p0016

class SumOfDigitsOfTwoPowers {

  val TWO = 2.0
  
  def getTwoPower(pow: Int): BigInt = {
    (0 until pow).toList.foldLeft[BigInt](1)((product, elem) => product * 2)
  }

  def getSumOfDigitsOfTwoPowers(pow: Int): Int = {
    getTwoPower(pow).toString().foldLeft[Int](0)( (sum, elem) => sum + elem.toString.toInt )
  }
}
