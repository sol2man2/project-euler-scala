package com.lge.swcec.projecteulerscala.p0009

class Solution {

  var pythagoreanTriplet = new PythagoreanTriplet

  def doCalc {
    val product = pythagoreanTriplet.getProductOfEquationAsPythagoreanFactorsSum(1000)
    println("===============================================================================")
    println("*************************          Problem 8          *************************")
    println("Find the greatest product of five consecutive digits in the 1000-digit number.")
    println("Answer: " + product)
    println("*************************          Problem 8          *************************")
    println("===============================================================================")
  }
}