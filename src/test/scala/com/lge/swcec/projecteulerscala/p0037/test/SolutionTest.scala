package com.lge.swcec.projecteulerscala.p0037.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0037.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 37: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}