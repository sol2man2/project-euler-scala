package com.lge.swcec.projecteulerscala.p0017

class GeneratorNumberToString {

  def getOneToThousand: Int = {
    val listString = (1 to 1000).map( num => getRealStringOf(num))
    val listNumber = listString.map(str => getTheNumberOfLetters(str))
    val sumOfLetters = listNumber.foldLeft(0)( (a, b) => a + b )
    sumOfLetters
  } 

  def getRealStringOf(number: Int): String = 
  {
    number match {
      case num if(num >= 0 && num < 10) => getOneDigitRealStringOf(num)
      case num if(num >= 10 && num < 21) => getTenDigitToTwentyRealStringOf(num)
      case num if(num >= 21 && num < 100) => getTenDigitRealStringOf(num / 10) + " " + getOneDigitRealStringOf(num % 10)
      case num if(num >= 100 && num < 1000) => {
        val remainUnder100 = (num % 100)
        val remainUnder10 = (num % 10)
        if( remainUnder100 == 0 && remainUnder10 == 0 ) {
          (
            getOneDigitRealStringOf(num / 100) + " hundred "
          ).trim()
        } else {
          (
            getOneDigitRealStringOf(num / 100) + " hundred and " +
            getRealStringOf(num % 100)
          ).trim()
        }
      }
      case num if(num >= 1000) => {
        (
          getRealStringOf(num / 1000) + " thousand " + 
          getRealStringOf(num % 1000)
        ).trim()
      }
    }
  }

  def getOneDigitRealStringOf(number: Int): String = {
    
    number match {
      case num if(num == 0) => ""
      case num if(num == 1) => "one"
      case num if(num == 2) => "two"
      case num if(num == 3) => "three"
      case num if(num == 4) => "four"
      case num if(num == 5) => "five"
      case num if(num == 6) => "six"
      case num if(num == 7) => "seven"
      case num if(num == 8) => "eight"
      case num if(num == 9) => "nine"
    }
  }

  def getTenDigitToTwentyRealStringOf(number: Int): String = {

    number match {
      case num if(num == 10) => "ten"
      case num if(num == 11) => "eleven"
      case num if(num == 12) => "twelve"
      case num if(num == 13) => "thirteen"
      case num if(num == 14) => "fourteen"
      case num if(num == 15) => "fifteen"
      case num if(num == 16) => "sixteen"
      case num if(num == 17) => "seventeen"
      case num if(num == 18) => "eighteen"
      case num if(num == 19) => "nineteen"
      case num if(num == 20) => "twenty"
    }
  }

  def getTenDigitRealStringOf(number: Int): String = {

    number match {
      case num if(num == 0) => ""
      case num if(num == 1) => throw new Exception("invald")
      case num if(num == 2) => "twenty"
      case num if(num == 3) => "thirty"
      case num if(num == 4) => "forty"
      case num if(num == 5) => "fifty"
      case num if(num == 6) => "sixty"
      case num if(num == 7) => "seventy"
      case num if(num == 8) => "eighty"
      case num if(num == 9) => "ninety"
    }
  }
  
  def getTheNumberOfLetters(number: String): Int = {
    number.replace(" ", "").length()
  }
}