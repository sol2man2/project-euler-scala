package com.lge.swcec.projecteulerscala.p0000

class Prime {
  var sieveNumber = 0
  var primes: Array[Int] = _

  def isPrime1(number: Long): Boolean = {
    number > primes.last match {
      case true => None; false
      //x      case _ => Some(primeSet.contains(number))
      //      case _ => Some(primes.contains(number))
      case _ => {
//        def searchPrime(lower: Int, upper: Int): Option[Boolean] = {
//          val middle = (lower + upper) / 2
//          if(primes(middle) == number) Some(true)
//          else if(primes(middle) < number) searchPrime(middle, upper)
//          else if(primes(middle) > number) searchPrime(lower, number)
//          else None
//        }
//        searchPrime(0, primes.length - 1)
        Some(primes.contains(number))
        primes.contains(number)
      }
    }
  }

  def length: Int = {
    primes.length
  }

  def isPrime(number: Long): Boolean = {
    getPrimeFactors(number) match {
      case Nil => true
      case _ => throw new Exception("not have prime at x")
    }
  }

  def getPrimeFactors(number: Long): List[Long] = {
    number > primes.last match {
      case true => {
        val factors = for {
          prime <- primes if(prime < math.sqrt(number).ceil.toInt)
        } yield {
          def findFactor(suspect: Long, factors: List[Long]): List[Long] = {
            if(suspect % prime == 0) findFactor(suspect / prime, prime :: factors)
            else factors
          }
          findFactor(number, Nil)
        }
        factors.toList.flatten
      }
      case false => {
        if(primes.contains(number) == true) List(number)
        else Nil
      }
    }
  }

  def getPrimeAt(index: Int): Int ={
    index < length match {
      case true => primes(index) 
      case false => 0
    }
  }

  def makePrimesThruSieveOfEratosThenes(number: Int): Array[Int] = {
    val suspects = new Array[Int](number + 1)
    suspects(0) = 1
    suspects(1) = 1

    for {
      idx <- 2 to (number / 2 + 1)
    } yield {
      var posi = idx * 2
      if(suspects(idx) == 0)while (posi <= number) {
        suspects(posi) = 1
        posi = posi + idx
      }
    }

    val pairs = (0 to number).zip(suspects).filter(pair => pair._2 == 0).toList
    primes = pairs.map(pair => pair._1).toArray
    sieveNumber = number
    primes
  }

}