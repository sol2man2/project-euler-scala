package com.lge.swcec.projecteulerscala.p0007

class Solution {

  val indexingPrimeNumbers = new IndexingPrimeNumbers

  def doCalc {
    val result = indexingPrimeNumbers.getPrimeAt(10000)
    println("===============================================================================")
    println("*************************          Problem 7          *************************")
    println("What is the 10 001st prime number?")
    println("Answer: " + result)
    println("*************************          Problem 7          *************************")
    println("===============================================================================")
  }
}
