package com.lge.swcec.projecteulerscala.p0001

import scala.collection.immutable._

class ThreeOrFiveList {
  
  def getSumOfAllTheMultiplesOf3Or5Below1000: Int = {
    getSumOfAllTheMultiplesOf3Or5Below(1000)
  }
  
  def getSumOfAllTheMultiplesOf3Or5Below(upperLimit: Int): Int = {
    getAllTheMultiplesOf3Or5Below(upperLimit).sum
  }
  
  def getAllTheMultiplesOf3Or5Below(upperLimit: Int): List[Int] = {
    (1 until upperLimit).filter(number => (number % 3 == 0 || number % 5 == 0)).toList
  }
}
