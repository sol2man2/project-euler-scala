package com.lge.swcec.projecteulerscala.p0021

import scala.collection.mutable.HashMap

class CalculationOfAmicableNumbers {

  val properDivisors = new ProperDivisors

  def getSumOfAmicableNumbers(number: Int) = {
    findAmicableNumbers(number).sum
  }
  
  def findAmicableNumbers(number: Int) = {
    List.range(1, number, 1).filter(isAmicableNumbers)
  }

  def getAmicableNumbers(number: Int): (Boolean, Int, Int) = {
    val dnumber = properDivisors.d(number)
    val ddnumber = properDivisors.d(dnumber)

    (number == ddnumber && number != dnumber, number, dnumber)
  }

  def isAmicableNumbers(number: Int): Boolean = {
    getAmicableNumbers(number)._1
  }
}