package com.lge.swcec.projecteulerscala.p0020.test

import scala.collection.Seq
import org.scalatest.FunSuite
import scala.collection.immutable.Map
import scala.reflect.Manifest
import scala.runtime.BoxedUnit
import java.lang.reflect.Method
import org.scalatest.BeforeAndAfter
import com.lge.swcec.projecteulerscala.p0020.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 20: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }
 
}