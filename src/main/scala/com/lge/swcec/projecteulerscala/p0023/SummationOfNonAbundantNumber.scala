package com.lge.swcec.projecteulerscala.p0023

import scala.collection.mutable.ListBuffer

class SummationOfNonAbundantNumber {

  def getSumOfNonAbundantNumber(): Int = {
    val upperLimitByMathematicalAnalysis = 28123

    val perfectNumber = new PerfectNumber 
    
    perfectNumber.getSumOfNonAbundantNumbers(upperLimitByMathematicalAnalysis)
  }
}