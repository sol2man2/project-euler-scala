package com.lge.swcec.projecteulerscala.p0017;

class Solution {

  val generatorNumberToString = new GeneratorNumberToString

  def doCalc {
  val howManyLettersWouldBeUsed = generatorNumberToString.getOneToThousand

    println("===============================================================================")
    println("*************************          Problem 17         *************************")
    println("If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?")
    println("Answer: " + howManyLettersWouldBeUsed)
    println("*************************          Problem 17         *************************")
    println("===============================================================================")
  }
}
