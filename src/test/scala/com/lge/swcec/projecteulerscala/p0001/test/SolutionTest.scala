/**
 *
 */
package com.lge.swcec.projecteulerscala.p0001.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0001._


/**
 * @author nature.song
 *
 */
class SolutionTest extends FunSuite with BeforeAndAfter {

	var solution: Solution = _

	before {
		solution = new Solution
	}

	test("Problem 1: test doCalc") {
		solution.doCalc();
	}

	after {
		
	}
}