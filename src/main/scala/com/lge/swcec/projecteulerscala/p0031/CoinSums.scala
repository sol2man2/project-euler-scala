package com.lge.swcec.projecteulerscala.p0031


class CoinSums {

  def getTheCaseOfTheWayToMakeL2: Int = {
    val _l2 = (0 to 1)
    val _l1 = (0 to 2)
    val _p50 = (0 to 4)
    val _p20 = (0 to 10)
    val _p10 = (0 to 20)
    val _p5  = (0 to 40)
    val _p2 = (0 to 100)
    val _p1 = (0 to 200)

    var cases = 0
    for( l2 <- _l2; l1 <- _l1; p50 <- _p50; p20 <- _p20; p10 <- _p10; p5 <- _p5; p2 <- _p2; p1 <- _p1 ) {
      if( (l2 * 200 + l1 * 100 + p50 * 50 + p20 * 20 + p10 * 10 + p5 * 5 + p2 * 2 + p1 * 1) == 200 ) {
        cases += 1
      }
    }

    cases
  }

}
