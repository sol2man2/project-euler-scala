package com.lge.swcec.projecteulerscala.p0002

class Solution {

	def doCalc {
		val fibonacciStream = new FibonacciStream
		val value = fibonacciStream.getSumOfFibonacciListBelow4000000
		println("===============================================================================")
		println("*************************          Problem 2          *************************")
		println("By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.")
		println("Answer: " + value)
		println("*************************          Problem 2          *************************")
		println("===============================================================================")

	}
}
