package com.lge.swcec.projecteulerscala.p0024

class Solution {

  val lexicographicPermutations = new LexicographicPermutations("0123456789")

  def doCalc {
    val megathLexicographicPermutations = lexicographicPermutations.getMegathLexicographicPermutations()

    println("===============================================================================")
    println("*************************          Problem 24         *************************")
    println("What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?")
    println("Answer: " + megathLexicographicPermutations)
    println("*************************          Problem 24         *************************")
    println("===============================================================================")
  }
}
