package com.lge.swcec.projecteulerscala.p0032

class Solution {

  val pandigitalProducts = new PandigitalProducts

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 32         *************************")
    println("Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.")

    val theSumOfNinePandigitalProducts = pandigitalProducts.getSumOfNinePandigitalProducts

    println("Answer: " + theSumOfNinePandigitalProducts)
    println("*************************          Problem 32         *************************")
    println("===============================================================================")
  }

}