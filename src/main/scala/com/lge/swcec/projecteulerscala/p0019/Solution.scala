package com.lge.swcec.projecteulerscala.p0019

class Solution {

  val howManySundayOnMonthFirst = new HowManySundayOnMonthFirst

  def doCalc {
    val theNumberOfSundays = howManySundayOnMonthFirst.getTheNumberOfSundays

    println("===============================================================================")
    println("*************************          Problem 19         *************************")
    println("How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?")
    println("Answer: " + theNumberOfSundays)
    println("*************************          Problem 19         *************************")
    println("===============================================================================")
  }

}