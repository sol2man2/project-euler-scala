package com.lge.swcec.projecteulerscala.p0005.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0005._

class MultiLcmTest extends FunSuite with BeforeAndAfter {

  var multiLcm: MultiLcm = _

  before {
    	multiLcm = new MultiLcm
	}

	test("Problem 5: test MultiLcm") {
		val result = multiLcm.getMultiLcm((1 to 20))

		assert(result == 232792560)
	}

	after {
	}
}