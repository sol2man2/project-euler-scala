package com.lge.swcec.projecteulerscala.p0038

class Solution {

  val pandigitalMultiples = new PandigitalMultiples

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 38         *************************")
    println("What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?")

    val theLargest1To9Pandigital9DigitNumberThatCanBeFormedAsTheConcatenatedProductOfAnInteger = pandigitalMultiples.getTheLargest1To9Pandigital9DigitNumberThatCanBeFormedAsTheConcatenatedProductOfAnInteger

    println("Answer: " + theLargest1To9Pandigital9DigitNumberThatCanBeFormedAsTheConcatenatedProductOfAnInteger)
    println("*************************          Problem 38         *************************")
    println("===============================================================================")
  }

}