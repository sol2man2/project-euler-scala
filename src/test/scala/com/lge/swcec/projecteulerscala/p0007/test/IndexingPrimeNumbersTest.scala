package com.lge.swcec.projecteulerscala.p0007.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0007._

class IndexingPrimeNumbersTest extends FunSuite with BeforeAndAfter {

  var indexingPrimeNumbers: IndexingPrimeNumbers = _

  before {
    indexingPrimeNumbers = new IndexingPrimeNumbers
  }

  test("Problem 7: test prime number in 10001th") {
    indexingPrimeNumbers.makePrimesThruSieveOfEratosThenes(1000000)
    val result = indexingPrimeNumbers.getPrimeAt(10000)

    assert(result == 104743)
  }

  after {
  }
}