package com.lge.swcec.projecteulerscala.p0033.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0033.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 33: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}