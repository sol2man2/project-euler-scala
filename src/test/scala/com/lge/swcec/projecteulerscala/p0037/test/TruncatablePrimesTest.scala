package com.lge.swcec.projecteulerscala.p0037.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0000.ListUtil
import com.lge.swcec.projecteulerscala.p0037.TruncatablePrimes
import com.lge.swcec.projecteulerscala.p0000.Prime
import scala.util.control.Breaks._
import scala.collection.mutable.ListBuffer

class TruncatablePrimesTest extends FunSuite with BeforeAndAfter {

  var truncatablePrimes: TruncatablePrimes = _

  before {
    truncatablePrimes = new TruncatablePrimes
  }

  test("the sum of 11 primes both left to right and right to left is ???") {
    val sum = truncatablePrimes.getTheSumOf11PrimesBothLeftToRightAndRightToLeft

    assert(sum == 748317)
  }

  ignore("study list") {
    val list = List(1, 2, 3, 4, 5)
    println("list: " + list.drop(2))

    println("slice: " + list.slice(0, 2))
    val string = "test"
    println("str len: " + string.length)
  }

  after {

  }

}

