package com.lge.swcec.projecteulerscala.p0027.test

import scala.collection.mutable.HashSet
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0003
import com.lge.swcec.projecteulerscala.p0027.PrimePool

class PrimePoolTest extends FunSuite with BeforeAndAfter {

  var primes: PrimePool = null

  before {
    primes = new PrimePool
  }

  after {
  }

  test("test initial sames (3,5)") {
    val newSet: HashSet[Int] = new HashSet
    newSet.add(3)
    newSet.add(5)

    primes.isPrime(3)
    primes.isPrime(5)

    assert(primes.primes == newSet)
  }

  test("test (3,5,7) is (3,5,7)") {
    val newSet: HashSet[Int] = new HashSet
    newSet.add(3)
    newSet.add(5)
    newSet.add(7)

    primes.isPrime(3)
    primes.isPrime(5)
    primes.isPrime(7)
    primes.isPrime(9)

    assert(primes.primes == newSet)
  }

  test("test that 1681 is not prime") {
    assert(primes.isPrime(1681) == false)
  }
  
}
