package com.lge.swcec.projecteulerscala.p0005.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0005._

class SolutionTest extends FunSuite with BeforeAndAfter {

	before {
	}

	test("Problem 5: test doCalc") {
		new Solution().doCalc
	}

	after {
	}
}
