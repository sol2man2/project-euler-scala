package com.lge.swcec.projecteulerscala.p0004.test

import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter

import com.lge.swcec.projecteulerscala.p0004._

class PalindromicTest extends FunSuite with BeforeAndAfter {

  var palindromic: Palindromic = _

  before {
    palindromic = new Palindromic
  }

  ignore("get 2 digits number's product with palindromic") {
    val result = palindromic.getProductOfTwo3DigitNumbers
    println("result: " + result)
  }

  test("get 3 digits number's product with palindromic") {
    val result = palindromic.getLargestPalindromicWithProductOf3DigitNumber
    println("result: " + result)

    assert(result == 906609)
  }

  after {
  }
}