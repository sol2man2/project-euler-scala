package com.lge.swcec.projecteulerscala.p0025

import scala.collection.mutable.ListBuffer
import com.lge.swcec.projecteulerscala.p0012._
import com.lge.swcec.projecteulerscala.p0002.FibonacciStream

class Fibonacci1 extends FibonacciStream {

  val twoDigit = 2
  val threeDigit = 3
  val thousandDigit = 1000

  def getThousandDigitFibonacciNumber(): (BigInt, Int) = {
    getNDigitFibonacciNumber(thousandDigit)
  }

  def getDigitNumberOf(number: BigInt): Int = {
    val str = number.toString
    str.length
  }
  
  def getNDigitFibonacciNumber(digit: Int): (BigInt, Int) = {
    
//    @tailrec
    def getNDigitFibonacci(prev: BigInt, current: BigInt, currentTerm: Int): (BigInt, Int) = {

      getDigitNumberOf(current) match {
        case n if n >= digit => (current, currentTerm)
        case _ => getNDigitFibonacci(current, prev + current, currentTerm + 1)
      }
    }
    
    getNDigitFibonacci(1, 1, 2)
  }

  def getNthTerm(digit: Int): BigInt = {
    
    0
  }

}
