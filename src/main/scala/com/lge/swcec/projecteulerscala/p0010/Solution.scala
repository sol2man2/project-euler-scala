package com.lge.swcec.projecteulerscala.p0010

class Solution {

  var rangePrimeNumbers = new RangePrimeNumbers

  def doCalc {

    val sum = rangePrimeNumbers.getTheSumSfAllThePrimesBelowTwoMillion

    println("===============================================================================")
    println("*************************          Problem 10         *************************")
    println("Find the sum of all the primes below two million.")
    println("Answer: " + sum)
    println("*************************          Problem 10         *************************")
    println("===============================================================================")
  }
}