package com.lge.swcec.projecteulerscala.p0018

class Solution {

  val maximumTotalFromTopToBottom = new MaximumTotalFromTopToBottom

  def doCalc {
    val findTheMaximumTotalFromTopToBottomOfTheTriangleBelow = maximumTotalFromTopToBottom.getMainMaximumTotal

    println("===============================================================================")
    println("*************************          Problem 18         *************************")
    println("Find the maximum total from top to bottom of the triangle below")
    println("Answer: " + findTheMaximumTotalFromTopToBottomOfTheTriangleBelow)
    println("*************************          Problem 18         *************************")
    println("===============================================================================")
  }
}
