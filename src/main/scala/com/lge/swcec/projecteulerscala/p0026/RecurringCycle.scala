package com.lge.swcec.projecteulerscala.p0026

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

class RecurringCycle {

  def beFoundSameCharInList(char: Char, list: List[Char], position: Int): Int = {
    list match {
      case hd :: tl if (char != hd) => beFoundSameCharInList(char, tl, position + 1)
      case _ => position + 1
    }
  }
  //
  //  def getRecurringCycle(list: List[Char]): Option[String] = {
  //
  //    def isRecurringCycleInHead(list: List[Char], interval: Int): Boolean = {
  //      val listOffirstElemInList = list.filter( _ % interval == 0 )
  //      val comparedResult = listOffirstElemInList.forall( elem => list.head == elem )
  //
  //      comparedResult
  //    }
  //
  //    def compareAllString(listOfList: List[String]): Option[String] = {
  //      listOfList match {
  //        case first :: second :: Nil if(first.startsWith(second)) => Option(first.mkString)
  //        case first :: second :: remain if(first == second) => compareAllString(listOfList.tail)
  //        case first :: second :: remain if(first != second) => None
  //        case _ => None
  //      }
  //    }
  //
  //    def findSuspect(list: List[Char], interval: Int): Option[String] = {
  //      isRecurringCycleInHead(list, interval) match {
  //        case true => {
  //          val stringList = list.mkString.grouped(interval).toList
  //          compareAllString(stringList)
  //        }
  //        case false => None
  //      }
  //    }
  //
  //    def getSuspect(list: List[Char]): Option[String] = {
  //      val loopScope = List.range(1, list.length, 1)
  //      for( index <- loopScope ) {
  //        findSuspect(list, index) match {
  //          case some => return some 
  //        }
  //      }
  //      None
  //    }
  //
  //    @tailrec
  //    def findRecurringCycle(list: List[Char]): Option[String] = {
  //      list match {
  //        case last :: Nil => None
  //        case _ => {
  //          getSuspect(list) match {
  //            case None => findRecurringCycle(list.tail)
  //            case cycle => cycle
  //          }
  //        }
  //      }
  //    }
  //
  //    list match {
  //      case Nil => None
  //      case last :: Nil => None
  //      case _ => {
  //        findRecurringCycle(list: List[Char]) match {
  //          case None => getRecurringCycle(list.tail)
  //          case cycle => cycle
  //        }
  //      }
  //    }
  //  }

  def getRecurringCycle(list: List[Char]): Option[String] = {

    def isRecurringCycleInHead(list: List[Char], interval: Int): Boolean = {
      var lb: ListBuffer[Char] = new ListBuffer

      for (index <- 0 until list.length) {
        if (index % interval == 0) {
          lb.append(list(index))
        }
      }

      val comparedResult = lb.toList.forall(elem => list.head == elem)

      comparedResult
    }

    def compareAllString(listOfList: List[String]): Option[String] = {
      listOfList match {
        case first :: second :: Nil if (first.startsWith(second)) => Option(first.mkString)
        case first :: second :: remain if (first == second) => compareAllString(listOfList.tail)
        case first :: second :: remain if (first != second) => None
        case _ => None
      }
    }

    def findSuspect(list: List[Char], interval: Int): Option[String] = {
      isRecurringCycleInHead(list, interval) match {
        case true => {
          val stringList = list.mkString.grouped(interval).toList
          compareAllString(stringList)
        }
        case false => None
      }
    }

    def getSuspect(list: List[Char]): Option[String] = {
      val loopScope = List.range(2, list.length / 2 + 1, 1)
      for (index <- loopScope) {
        findSuspect(list, index) match {
          case Some(number) => return Some(number)
          case None =>
        }
      }
      None
    }

    @tailrec
    def findRecurringCycle(list: List[Char]): Option[String] = {
      list match {
        case Nil => None
        case _ => {
          getSuspect(list) match {
            case None => findRecurringCycle(list.tail)
            case cycle => cycle
          }
        }
      }
    }

    list match {
      case Nil => None
      case _ => {
        findRecurringCycle(list: List[Char]) match {
          case None => getRecurringCycle(list.tail)
          case cycle => cycle
        }
      }
    }
  }

  def getRecurringCycle(str: String): Option[String] = {

    //    println("source number: " + str.toList)
    getRecurringCycle(str.toList)
  }

  def getRecurringCycle(number: BigDecimal): Option[String] = {

    val str = number.toString
    getRecurringCycle(str.substring(2, str.length - 1).toList)
  }
}
