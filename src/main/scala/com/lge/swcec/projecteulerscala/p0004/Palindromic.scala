package com.lge.swcec.projecteulerscala.p0004

class Palindromic {

  def findLargestPalindromic(start: BigInt, upperLimit: BigInt): BigInt = {
    BigInt(0)
  }
  
  def getLargestPalindromicWithProductOf3DigitNumber: Int = {
    getLargestPalindromicWithProductOfDigitNumber(3)
  }

  def getLargestPalindromicWithProductOfDigitNumber(digit: Int): Int = {
    getProductOfTwoNumbers(digit).max
  }
  
  def getLargestPalindromic(digit: Int): Int = {
    getProductOfTwoNumbers(digit).max
  }

  def getProductOfTwo3DigitNumbers(): List[Int] = {
    getProductOfTwoNumbers(3)
  }

  def getProductOfTwoNumbers(digit: Int): List[Int] = {
    val begin = math.pow(10.0, digit - 1).toInt
    val end = math.pow(10.0, digit).toInt
    val list = (begin until end).toList

    for {
      multiplicant <- list 
      multiplier <- list
      product = multiplicant * multiplier if (isPalindromic(product))
    } yield product
  }

  def isPalindromic(number: Int): Boolean = {
    val forward = number.toString
    val backward = forward.reverse
    forward == backward
  }
}