package com.lge.swcec.projecteulerscala.p0013.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0013._

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
  }

  test("Problem 13: test doCalc") {
    new Solution().doCalc
  }

  after {
  }
}
