package com.lge.swcec.projecteulerscala.p0013

class Solution {

  val fiftyDigitNumbers = new FiftyDigitNumbers

  def doCalc {
  val sum = fiftyDigitNumbers.sum

    println("===============================================================================")
    println("*************************          Problem 13         *************************")
    println("Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.")
    println("Answer: " + sum)
    println("*************************          Problem 13         *************************")
    println("===============================================================================")
  }
}