package com.lge.swcec.projecteulerscala.p0020.test

import scala.collection.Seq
import org.scalatest.FunSuite
import scala.collection.immutable.Map
import scala.reflect.Manifest
import scala.runtime.BoxedUnit
import java.lang.reflect.Method
import org.scalatest.BeforeAndAfter
import com.lge.swcec.projecteulerscala.p0020.SummationOfFactorialDigit
import com.lge.swcec.projecteulerscala.p0020.FactorialOnBigInt
import com.lge.swcec.projecteulerscala.p0020.FactorialOnBigInt

class SummationOfFactorialDigitTest extends FunSuite with BeforeAndAfter {

  var summationOfFactorialDigit: SummationOfFactorialDigit = _

  before {
    summationOfFactorialDigit = new SummationOfFactorialDigit
  }

  test("test whether 10! is 3628800") {
    val factorial = new FactorialOnBigInt
    val tenFactorial = factorial.getNFactorial(10)
    
    assert(3628800 == tenFactorial)
  }

  test("test whether 100! is 3628800") {
    val factorial = new FactorialOnBigInt
    val hundredFactorial = factorial.getNFactorial(100)
    val hundredFactorialString = hundredFactorial.toString

    assert("93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000" == hundredFactorialString)
  }

  test("test whether sum of 10!'s digit is 27") {
    val sum = summationOfFactorialDigit.getSumOfFactorialDigit(10)

    println("Sum of 10!'s digit: " + sum)

    assert(27 == sum)
  }

  test("test Sum of Factorial Digit is 648") {
    val sum = summationOfFactorialDigit.getSumOfFactorialDigit(100)

    println("Sum of 100!'s digit: " + sum)

    assert(648 == sum)
  }

  after {    
  }

}