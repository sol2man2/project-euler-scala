package com.lge.swcec.projecteulerscala.p0027.test

import java.util.concurrent.atomic.AtomicReference
import org.scalatest.BeforeAndAfter
import scala.collection.immutable.Map
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0027.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 27: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}