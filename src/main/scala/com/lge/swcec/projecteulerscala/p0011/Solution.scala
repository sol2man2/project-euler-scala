package com.lge.swcec.projecteulerscala.p0011

class Solution {

  var twentyByTwentyMatrix = new TwentyByTwentyMatrix

  def doCalc {
    val max = twentyByTwentyMatrix.getProductMax(4)
    println("===============================================================================")
    println("*************************          Problem 11         *************************")
    println("What is the greatest product of four adjacent numbers in any direction (up, down, left, right, or diagonally) in the 20x20 grid?")
    println("Answer: " + max)
    println("*************************          Problem 11         *************************")
    println("===============================================================================")
  }
}