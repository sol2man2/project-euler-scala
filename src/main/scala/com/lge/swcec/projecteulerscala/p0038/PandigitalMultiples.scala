package com.lge.swcec.projecteulerscala.p0038

import com.lge.swcec.projecteulerscala.p0000.ListUtil
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._

class PandigitalMultiples {

  def getTheLargest1To9Pandigital9DigitNumberThatCanBeFormedAsTheConcatenatedProductOfAnInteger: Int = {
    get1To9Pandigital9DigitNumbersThatCanBeFormedAsTheConcatenatedProductOfAnInteger.map(
      get9DigitNumbersThatCanBeFormedAsTheConcatenatedProductOfAnInteger).max
  }

  def get9DigitNumbersThatCanBeFormedAsTheConcatenatedProductOfAnInteger(number: Int): Int = {
    val listBuffer = new ListBuffer[Int]
    var length = 0
    breakable {
      for {
        index <- 1 to 9
      } yield {
        val multiple = number * index
        listBuffer.append(multiple)
        length = length + multiple.toString.length
        if (length >= 9) break
      }
    }
    listBuffer.toList.map(elem => elem.toString).mkString.toInt
  }

  def get1To9Pandigital9DigitNumbersThatCanBeFormedAsTheConcatenatedProductOfAnInteger: List[Int] = {
    val list = (1 to 1000000).toList
    val pandigitalMultiples = list.filter(number => {
      def isPandigitalMultiples: Boolean = {
        def checkPandigitalMultiples(suspect: Int, multiplier: Int, length: Int): Boolean = {
          var length = 0
          val listBuffer = new ListBuffer[Int]
          breakable {
            for {
              index <- 1 to 9
            } yield {
              val multiple = suspect * index
              listBuffer.append(multiple)
              length = length + multiple.toString.length
              if (length >= 9) break
            }
          }
          length match {
            case 9 => {
              val concatenated = listBuffer.toList.map(elem => elem.toString).mkString
              val list = "123456789"
              val diff = list.diff(concatenated)
              diff == ""
            }
            case _ => false
          }
        }
        checkPandigitalMultiples(number, 1, 0)
      }
      isPandigitalMultiples
    })
    pandigitalMultiples
  }
}