package com.lge.swcec.projecteulerscala.p0022

import java.io.File
import java.io.FileReader
import java.io.BufferedReader

class SummationOfSortedNamesScores {

  def getSumOfSortedNamesScores(fileName: String) = {
    val listNames = getNamesListFromFile(fileName)
    getNamesListScores(listNames)
  }

  def getNamesListFromFile(fileName: String) = {
    val list = getNamesList(getNamesFromFile(fileName))

    list.sortWith((x, y) => x < y)
  }
  
  def getNamesList(string: String): List[String] = {
    val listSplited = string.split(',').toList
    val listStrStripped = listSplited.map(str => {
      val listTemp = str.split("\"").toList
      listTemp(1)
    })
    listStrStripped
  }
  
  def getNamesListScores(listNames: List[String]) = {
    val listNamesScores = for( i <- 0 until listNames.length ) yield getNamesScores(i + 1, listNames(i))
    listNamesScores.sum
  }
  
  def getNamesScores(index: Int, name: String) = {
    index * getSumOfNamesWorth(name)
  }

  def getSumOfNamesWorth(name: String) = {
    val listName = name.toUpperCase().toList
    val sumOfNamesWorth = getNamesWorth(listName)
    sumOfNamesWorth
  }
  
  def getNamesWorth(listName: List[Char]) = {
    val worth = listName.foldLeft(0)((a, b) => {
      a + b.toByte - 'A'.toByte + 1
    })
    worth
  }

  def getNamesFromFile(fileName: String) = {
    val file = new File(fileName)
    val fileSize = file.length()

    var fileReader: FileReader = null
    var bufferedReader: BufferedReader = null
    
    try {
      fileReader = new FileReader(file)
      bufferedReader = new BufferedReader(fileReader)

      bufferedReader.readLine()
    }
    finally {
      bufferedReader.close()
      fileReader.close()
    }
  }
}
