package com.lge.swcec.projecteulerscala.p0031.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0031.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 31: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}