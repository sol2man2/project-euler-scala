package com.lge.swcec.projecteulerscala.p0026

import scala.annotation.tailrec

class ReciprocalCycles {

  val twoDigit = 2
  val threeDigit = 3
  val thousand = 1000
  
  val recurringCycle = new RecurringCycle

  def getTheNumberOfLongestRecurringCycleUnder1000(digit: Int): Any = {
    getTheNumberOfLongestRecurringCycle(thousand, digit)
  }

  def getTheNumberOfLongestRecurringCycle(upperLimit: Int, digit: Int): Any = {
    val limit = upperLimit
    val oneToLimit = List.range(3, limit, 2).to[List]
    val onepointoneToLimit = oneToLimit.map( getReciprocalNumber(_, digit) )

    val reciprocalCycle = 
    for( elem <- onepointoneToLimit ) yield {
      recurringCycle.getRecurringCycle(elem)
    }

    val lengthList = 
    reciprocalCycle.map( elem => {
      elem match {
        case None => 0
        case _ => elem.get.length
      }
    })

    (lengthList.zipWithIndex.max._1 + 1, lengthList.zipWithIndex.max._2 * 2 + 1)
  }

  def getReciprocalNumber(number: Int, digit: Int): String = {
    @tailrec
    def getReciprocal(numerator: Int, denominator: Int, quotient: List[Char], digit: Int): List[Char] = {
      digit match {
        case n if( digit <= 0 ) => quotient.reverse
        case n if( numerator == 0 ) => quotient.reverse
        case n if( numerator >= denominator ) => getReciprocal(numerator%denominator * 10, denominator, (numerator/denominator + '0').toChar :: quotient, digit - 1)
        case n if( numerator < denominator ) => getReciprocal(numerator * 10, denominator, '0' :: quotient, digit - 1)
      }
    }
    getReciprocal(10, number, Nil, digit).mkString
  }

}
