package com.lge.swcec.projecteulerscala.p0020

class FactorialOnBigInt {

  def getNFactorial(number: BigInt): BigInt = {
    number match {
      case n if(n == 0 || n == 1) => 1
      case n if(n != 1) => number * getNFactorial(number - 1)
    }
  }
}