package com.lge.swcec.projecteulerscala.p0003.test

import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import com.lge.swcec.projecteulerscala.p0003._
import com.lge.swcec.projecteulerscala.p0000.Prime

class PrimeFactorsTest extends FunSuite with BeforeAndAfter {

  val EXAMPLE_INPUT = 13195
  val PROBLEM_INPUT = 600851475143L

  var primeFactors: PrimeFactors = _

  before {
    primeFactors = new PrimeFactors
  }

  test("Problem 3: test get prime factors of 600851475143L") {
    val number = 600851475143L
    val suspectSmallFactor = math.sqrt(number).ceil.toInt
    primeFactors.makePrimesThruSieveOfEratosThenes(suspectSmallFactor)

    var result = primeFactors.getPrimeFactors(number).last

    assert(result == 6857)
  }

  ignore("Problem 3: test get prime factors of 36") {
    val list = primeFactors.getPrimeFactors(36)

    assert(list == List(2, 2, 3, 3))
  }

  ignore("Problem 3: test get prime factors of 76576500") {
    val list = primeFactors.getPrimeFactors(76576500)

    println("76576500: " + list)
    println("76576500:size: " + list.size)

    assert(list == List(2, 2, 3, 3, 5, 5, 5, 7, 11, 13, 17))
  }

  ignore("Problem 3: test get prime factors of 13195") {
    val list = primeFactors.getPrimeFactors(EXAMPLE_INPUT)

    assert(list == List(5, 7, 13, 29))
  }

  ignore("Problem 3: test get prime factors of 600851475143") {
    val list = primeFactors.getPrimeFactors(PROBLEM_INPUT)

    assert(list == List(71, 839, 1471, 6857))
  }

  ignore("Problem 3: test get prime factors of 906609") {
    val list = primeFactors.getPrimeFactors(906609)

    println("906609 list: " + list)
    assert(list == List(3, 11, 83, 331))
  }

  ignore("Problem 3: test get prime factors of 528") {
    val list = primeFactors.getPrimeFactors(528)

    println("528 list: " + list)
    assert(list == List(2, 2, 2, 2, 3, 11))
  }

  ignore("Problem 3: test get prime factors of 131584753") {
    val list = primeFactors.getPrimeFactors(131584753)

    println("131584753 list: " + list)
    assert(list == List(3, 11, 83, 331)) //	480
  }

  ignore("Problem 3: test get prime factors of 134734320") {
    val list = primeFactors.getPrimeFactors(134734320)

    println("134734320 list: " + list)
    assert(list == List(3, 11, 83, 331)) //	480
  }

  ignore("Problem 3: test get prime factors of 137373600") {
    val list = primeFactors.getPrimeFactors(137373600)

    println("137373600 list: " + list)
    assert(list == List(2, 2, 2, 2, 2, 3, 5, 5, 7, 13, 17, 37))
  }

  after {
  }
}