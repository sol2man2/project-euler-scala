package com.lge.swcec.projecteulerscala.p0022.test

import scala.Array.canBuildFrom

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0022.SummationOfSortedNamesScores

class SummationOfSortedNamesScoresTest extends FunSuite with BeforeAndAfter {

  val strNames = """ "COLIN", "JAMES" ,"NATURE" """
  val listNames = List("COLIN", "JAMES", "NATURE")
  var summationOfSortedNamesScores: SummationOfSortedNamesScores = _ 
  
  before {
    summationOfSortedNamesScores = new SummationOfSortedNamesScores
  }

  ignore("test parsing name") {
    val list = summationOfSortedNamesScores.getNamesList(strNames)
    println("list:" + list)
    assert(true)
  }

  ignore("test COLIN's worth") {
    val list = summationOfSortedNamesScores.getNamesList(""" "COLIN" """)
    val listNamesScores = summationOfSortedNamesScores.getSumOfNamesWorth(list(0))

    assert(53 == listNamesScores)
  }

  ignore("test JAMES's worth") {
    val list = summationOfSortedNamesScores.getNamesList(""" "JAMES" """)
    val listNamesScores = summationOfSortedNamesScores.getSumOfNamesWorth(list(0))

    assert(48 == listNamesScores)
  }

  ignore("test NATURE's worth") {
    val list = summationOfSortedNamesScores.getNamesList(""" "NATURE" """)
    val listNamesScores = summationOfSortedNamesScores.getSumOfNamesWorth(list(0))

    assert(79 == listNamesScores)
  }

  ignore("test name's score") {
    val list = summationOfSortedNamesScores.getNamesList(""" "COLIN" """)
    println("single name list: " + list)
    val listNamesScores = summationOfSortedNamesScores.getNamesScores(938, list(0))

    assert(49714 == listNamesScores)
  }
  
  ignore("test sample name list's scores") {
    val sum = summationOfSortedNamesScores.getNamesListScores(listNames)

    assert( 386 == sum )
  }
  
  test("test name list's scores") {
    val sum = summationOfSortedNamesScores.getNamesListScores(summationOfSortedNamesScores.getNamesListFromFile("names.txt"))

    assert( 871198282 == sum )
  }
  
  ignore("test read names from file") {
    val list = summationOfSortedNamesScores.getNamesFromFile("names.txt")
    println("names list: " + list)
  }

  ignore("test read names list from file") {
    val list = summationOfSortedNamesScores.getNamesListFromFile("names.txt")
    println("names list: " + list)
  }

  ignore("test string manipulation") {
    val listSplited = strNames.split(',').toList
    println("Splited: " + listSplited)
    
    val listStrStripped = listSplited.map(str => {
      val listTemp = str.split("\"").toList
//      println("length(" + listTemp.length + "): " + listTemp(1))
      listTemp(1)
    })
    
    println("Stripped: " + listStrStripped)
    
  }

  ignore("test whether sum of sorted names scores is ???") {
//    assert(false)
  }

  after {
    
  }

}