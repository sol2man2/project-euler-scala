package com.lge.swcec.projecteulerscala.p0036

class Solution {

  val doubleBasePalindromes = new DoubleBasePalindromes

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 36         *************************")
    println("Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.")

    val theSumOfAllNumbersLessThanOneMillionWithPalindromicBothBase10AndBase2 = doubleBasePalindromes.getTheSumOfAllNumbersLessThanOneMillionWithPalindromicBothBase10AndBase2

    println("Answer: " + theSumOfAllNumbersLessThanOneMillionWithPalindromicBothBase10AndBase2)
    println("*************************          Problem 36         *************************")
    println("===============================================================================")
  }

}