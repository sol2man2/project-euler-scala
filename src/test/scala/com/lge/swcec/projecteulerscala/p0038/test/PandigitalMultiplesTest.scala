package com.lge.swcec.projecteulerscala.p0038.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import scala.util.control.Breaks._
import com.lge.swcec.projecteulerscala.p0038.PandigitalMultiples
import scala.collection.mutable.ListBuffer
import scala.tools.nsc.transform.Flatten

class PandigitalMultiplesTest extends FunSuite with BeforeAndAfter {

  var pandigitalMultiples: PandigitalMultiples = _

  before {
    pandigitalMultiples = new PandigitalMultiples
  }

  test("the largest 1 to 9 pandigital 9 digit number that can be formed as the concatenated product of an integer is ???") {
    val largest = pandigitalMultiples.getTheLargest1To9Pandigital9DigitNumberThatCanBeFormedAsTheConcatenatedProductOfAnInteger

    assert(largest == 932718654)
  }

  ignore("study list") {
    val list = List("1", "2", "3")
    println(":" + list.mkString)
    val concatenated = List("2", "3", "1")
    val result = list.diff(concatenated)
    
    assert(result == Nil)
    
    val a = List("123")
    val b = List("2314")
    
    val result1 = a.mkString.diff(b.mkString)
    println("result1: " + result1)
    println(":" + result1.getClass)
    println("is nil: " + (result1.toList == Nil))
    println("is \"\": " + (result1 == ""))

    println(":" + a.mkString)
  }

  after {

  }

}

