package com.lge.swcec.projecteulerscala.p0012.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0012._

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
  }

  test("Problem 12: test doCalc") {
    new Solution().doCalc
  }

  after {
  }
}