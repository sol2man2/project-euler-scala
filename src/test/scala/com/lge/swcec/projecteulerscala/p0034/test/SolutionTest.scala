package com.lge.swcec.projecteulerscala.p0034.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0034.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 34: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}