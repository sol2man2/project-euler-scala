package com.lge.swcec.projecteulerscala.p0019.test

import scala.reflect.Manifest
import org.joda.time.DateTime
import org.joda.time.LocalTime
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0019.HowManySundayOnMonthFirst

class HowManySundayOnMonthFirstTest extends FunSuite with BeforeAndAfter {

  var date: DateTime = _

  before {
    date = new DateTime
  }

  test("show joda time") {
    println("date: " + date.toString())
    println("year, month, day: " + date.year.getAsString + "," + date.monthOfYear.getAsString + "," + date.dayOfMonth.getAsString)
    assert(true)
  }

  test("show joda time year, month, day") {
    println("time: " + date.toLocalTime.toString)
    println("time: " + date.toDateTime.toString())
    Thread.sleep(1000)
    println("date: " + date.toString())
    assert(true)
  }

  test("show me that Jan first 1900 is monday") {
    var d = new DateTime(1900, 1, 1, 1, 1, 1)
    println("1900-01-01: " + d.toDateTime.toString())
    println("1900-01-01' day: " + d.dayOfWeek.getAsText)
    assert(d.dayOfWeek.getAsText == "월요일")
    d = new DateTime(1900, 1, 1, 0, 0, 0)
    println("1900-01-01: " + d.toDateTime.toString())
    d = new DateTime(1900, 1, 1, 23, 59, 59)
    println("1900-01-01: " + d.toDateTime.toString())
    assert(true)
  }

  test("explore about Jan first 1900") {
    val howMannySundayOnMonthFirst = new HowManySundayOnMonthFirst
    val firstSundayCount = howMannySundayOnMonthFirst.getTheNumberOfSundays 

    println("Total Sunday's Count: " + firstSundayCount)
    assert(171 == firstSundayCount)
  }

  after {
    
  }
  
}
