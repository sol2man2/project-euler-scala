package com.lge.swcec.projecteulerscala.p0027

class Solution {

  val quadraticPrimes = new QuadraticPrimes

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 27         *************************")
    println("Find the product of the coefficients, a and b, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n = 0")

    val theNumberOflongestReciprocalCyclesUnder2000 = quadraticPrimes.findProductOfCoefficientsWithMaxinumPrime(1000, 1000)

    println("Answer: " + theNumberOflongestReciprocalCyclesUnder2000)
    println("*************************          Problem 27         *************************")
    println("===============================================================================")
  }

}