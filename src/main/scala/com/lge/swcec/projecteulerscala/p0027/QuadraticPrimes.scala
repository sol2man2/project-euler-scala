package com.lge.swcec.projecteulerscala.p0027

import scala.annotation.tailrec

class QuadraticPrimes {

  val primes = new PrimePool

  def getLastPrimeIndex(alpha: Int, beta: Int): Long = {
    val expression = QuadraticExpression.get(alpha, beta)

    0
  }

  def getLastIndexOnPrimeFrom(expression: QuadraticExpression, from: Long): Long = {

    @tailrec
    def getLastPrime(number: Long): Long = {
      primes.isPrime(Math.abs(expression.evaluate(number))) match {
        case true => getLastPrime(number + 1)
        case false => number
      }
    }

    getLastPrime(from)
  }

  def arePrimeFromTo(expression: QuadraticExpression, from: Long, to: Long): Boolean = {
    val range = List.range(from, to + 1, 1)
    range.forall(number => primes.isPrime(Math.abs(expression.evaluate(number))))
  }

  def findProductOfCoefficientsWithMaxinumPrime(alphaEfficient: Int, betaEfficient: Int): (Long, Long, Long) = {
    val alphas = -alphaEfficient to alphaEfficient
    val betas = -betaEfficient to betaEfficient

    var maxIndexOfPrime: Long = 0
    var maxAlpha = 0
    var maxBeta = 0

    for (alpha: Int <- alphas.view; beta: Int <- betas.view) {
      val expression = QuadraticExpression.get(alpha, beta)

      val lastIndexOnPrimes = getLastIndexOnPrimeFrom(expression, 0)

      if(maxIndexOfPrime <= lastIndexOnPrimes - 1) {
        maxIndexOfPrime = lastIndexOnPrimes - 1
        maxAlpha = alpha
        maxBeta = beta

        println("<found> alpha: " + maxAlpha + ", beta: " + maxBeta + ", lastIndexOfPrime: " + maxIndexOfPrime)
      }
    }
    
//    for (alpha: Int <- alphas.view; beta: Int <- betas.view) {
//      val expression = QuadraticExpression.get(alpha, beta)
//
//      val suspectIndex = maxIndexOfPrime
//      val suspectPrime = Math.abs(expression.evaluate(suspectIndex))
//      val bPrime = primes.isPrime(suspectPrime)
//
//      bPrime match {
//        case true => {
//          val bAllPrime = arePrimeFromTo(expression, 0, suspectIndex)
//
//          bAllPrime match {
//            case true => {
//              val lastIndexOfPrime: Long = getLastIndexOnPrimeFrom(expression, suspectIndex + 1)
//
//              (lastIndexOfPrime != suspectIndex + 1) match {
//                case true => {
//                  maxIndexOfPrime = lastIndexOfPrime - 1
//                  maxAlpha = alpha
//                  maxBeta = beta
//                  println("<found> alpha: " + alpha + ", beta: " + beta + ", maxIndexOfPrime: " + maxIndexOfPrime)
//                }
//                case false =>
//              }
//            }
//            case false => {
//            }
//          }
//        }
//        case false => {
//        }
//      }
//    }

    (maxAlpha, maxBeta, maxIndexOfPrime)
  }
}