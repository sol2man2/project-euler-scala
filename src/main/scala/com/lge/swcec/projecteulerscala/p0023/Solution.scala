package com.lge.swcec.projecteulerscala.p0023

class Solution {

  val summationOfNonAbundantNumber = new SummationOfNonAbundantNumber

  def doCalc {
    val sumOfNonAbundantNumber = summationOfNonAbundantNumber.getSumOfNonAbundantNumber()

    println("===============================================================================")
    println("*************************          Problem 23         *************************")
    println("Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.")
    println("Answer: " + sumOfNonAbundantNumber)
    println("*************************          Problem 23         *************************")
    println("===============================================================================")
  }
}
