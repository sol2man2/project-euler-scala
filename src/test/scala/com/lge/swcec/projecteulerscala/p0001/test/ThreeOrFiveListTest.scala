package com.lge.swcec.projecteulerscala.p0001.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0001._

class ThreeOrFiveListTest extends FunSuite with BeforeAndAfter {

  var threeOrFiveList: ThreeOrFiveList = _

  before {
    threeOrFiveList = new ThreeOrFiveList
  }

  test("Problem 1: test ThreeOrFiveList") {
    val sum = threeOrFiveList.getSumOfAllTheMultiplesOf3Or5Below1000
    
    assert(sum == 233168)
  }

  after {
  }
}