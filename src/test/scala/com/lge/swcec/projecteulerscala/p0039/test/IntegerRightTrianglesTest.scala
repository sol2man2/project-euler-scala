package com.lge.swcec.projecteulerscala.p0039.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import scala.util.control.Breaks._
import scala.collection.mutable.ListBuffer
import scala.tools.nsc.transform.Flatten
import com.lge.swcec.projecteulerscala.p0039.IntegerRightTriangles

class IntegerRightTrianglesTest extends FunSuite with BeforeAndAfter {

  var integerRightTriangles: IntegerRightTriangles = _

  before {
    integerRightTriangles = new IntegerRightTriangles
  }

  test("for which value of p ≤ 1000, is the number of solutions maximised is 840") {
    val maxP = integerRightTriangles.getTheNumberOfSolutionsMaximisedForWhichValueOfPLessThen1000

    println("pValue: " + maxP)
    
    assert(maxP == 840)
  }

  ignore("study list") {
    val list = List(1,5,3,7,2)
    list.sorted
    println("sorted: " + list.sorted)
  }

  after {

  }
}
