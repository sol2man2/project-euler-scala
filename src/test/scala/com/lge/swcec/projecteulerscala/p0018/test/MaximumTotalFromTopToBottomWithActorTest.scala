package com.lge.swcec.projecteulerscala.p0018.test

import scala.actors.Actor._
import scala.actors.Actor
import scala.actors.TIMEOUT

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0018.MaximumTotalFromTopToBottomWithActor
import com.lge.swcec.projecteulerscala.p0018.TriangleData

class MaximumTotalFromTopToBottomWithActorTest extends FunSuite with BeforeAndAfter {

  var maximumTotalFromTopToBottomWithActor: MaximumTotalFromTopToBottomWithActor = _

  before {
    maximumTotalFromTopToBottomWithActor = new MaximumTotalFromTopToBottomWithActor
  }

  ignore("Problem 18: test list's size") {
    val lists = TriangleData.listsSampleTriangle
    val size = lists.size

    assert(Math.pow(2.0, 3.0) == 8.0)
    assert(size == 4)
    assert(TriangleData.listsMainTriangle.size == 15)
  }

  ignore("Problem 18: test the number of paths") {
    val lists = TriangleData.listsSampleTriangle
    val size = lists.size
    val nrOfPaths = Math.pow(2.0, (size - 1).toFloat).toInt
    
    assert(nrOfPaths == 8)
  }

  case class TriangleLists(gatherer: Actor, index: Int, sum: Int, lists: List[List[Int]])
  case class TriangleListsPill()

//  class MaximumTotalActor(gatherer: Actor, initLists: List[List[Int]]) extends Actor {
  class MaximumTotalActor extends Actor {
    
    def act() {
      var fLoop = true
      loopWhile(fLoop) {
        react {
          case TriangleListsPill => {
            println("accept Pill")
            fLoop = false
          }            
          case TriangleLists(gatherer, index, sum, ls) => {
            ls match {
              case Nil => {
                gatherer !! sum
              }
              case headList :: tailList => {
                this !! TriangleLists(gatherer, index, sum + headList(index), tailList)
                this !! TriangleLists(gatherer, index + 1, sum + headList(index), tailList)
              }
            }
          }
        }
      }
    }
    println("MaximumTotalActor die?")
  }

  class TriangleMaxTotal(requester: Actor) extends Actor {

    var maxTotal = 0
    var szList: Int = 0

    var finderOfMaximumTotal: MaximumTotalActor = _

    def getMaximumTotal(lists: List[List[Int]]) = {
      szList = lists.length
      finderOfMaximumTotal = new MaximumTotalActor
      finderOfMaximumTotal.start

//      finderOfMaximumTotal !! TriangleLists(this, 0, 0, lists)
      finderOfMaximumTotal !! TriangleLists(this, 0, 0, lists)
      
      
    }

    def act() {
      var s: scala.actors.OutputChannel[Any] = null
      var nrPathCase = Math.pow(2.0, szList.toFloat - 1.0) * 2

      
      while(nrPathCase > 0) {
        receiveWithin(3000) {
          case sum: Int => {
            nrPathCase -= 1
            maxTotal = if(maxTotal < sum) sum else maxTotal
            s = sender
          }
          case TIMEOUT => {
            println("Timeout!")
          } 
        }
      }
      println("huk:")
      finderOfMaximumTotal !! TriangleListsPill
//      fin

      requester ! maxTotal
      assert(maxTotal == 23)
    }
  }

  test("Problem 18: test sample's maximum total with actor") {
    val triangleMaxTotal = new TriangleMaxTotal(self)
    triangleMaxTotal.start
    triangleMaxTotal.getMaximumTotal(TriangleData.listsSampleTriangle)
    
    receiveWithin(5000) {
      case TIMEOUT => println("timeout!!!")
      case msg => println("msg!: " + msg)
    }
  }

  after {
  }

}
