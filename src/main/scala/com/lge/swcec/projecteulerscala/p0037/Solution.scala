package com.lge.swcec.projecteulerscala.p0037

class Solution {

  val truncatablePrimes = new TruncatablePrimes

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 37         *************************")
    println("Find the sum of the only eleven primes that are both truncatable from left to right and right to left.")

    val theSumOf11PrimesBothLeftToRightAndRightToLeft = truncatablePrimes.getTheSumOf11PrimesBothLeftToRightAndRightToLeft

    println("Answer: " + theSumOf11PrimesBothLeftToRightAndRightToLeft)
    println("*************************          Problem 37         *************************")
    println("===============================================================================")
  }

}