package com.lge.swcec.projecteulerscala.p0006.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0006._

class SolutionTest extends FunSuite with BeforeAndAfter {

	before {
	}

	test("Problem 6: test doCalc") {
		new Solution().doCalc
	}

	after {
	}
}