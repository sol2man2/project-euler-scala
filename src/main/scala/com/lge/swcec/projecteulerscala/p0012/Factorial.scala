package com.lge.swcec.projecteulerscala.p0012

class Factorial {

  def getNFactorial(number: Long): Long = {
    number match {
      case n if(n == 0 || n == 1) => 1
//      case n if(n == 0 || n == 1) => throw new Exception("getNFactorial")
      case n if(n != 1) => number * getNFactorial(number - 1)
    }
  }

  def getNFactorialWithTailRecursion(number: Long): Long = {
    getNFactorialTailRecursion(number, 1)
  }

  def getNFactorialTailRecursion(number: Long, product: Long): Long = {
    number match {
      case n if(n == 0 || n == 1) => product
//      case n if(n == 0 || n == 1) => throw new Exception("getNFactorialTailRecursion")
      case n if(n != 1) => getNFactorialTailRecursion( number - 1, product * number )
    }
  }

//  def getNFactorialTailRecursion(product: Long, number: Long): Long = {
//    number match {
//      case n if(n == 0 || n == 1) => product
////      case n if(n == 0 || n == 1) => throw new Exception("getNFactorialTailRecursion")
//      case n if(n != 1) => getNFactorialTailRecursion( product * number, number - 1 )
//    }
//  }

  def normal(number:Int) : Int = {
    if (number == 1) throw new Exception("normal")
    else number * normal (number - 1)
  }
  
//  def tail(number: Int, accumulator: Int) : Int = 
//    if(number == 1) throw new Exception("tail")
//    else tail(number - 1, number * accumulator)

  def tail(accumulator: Int, number: Int) : Int = 
    if(number == 1) throw new Exception("tail")
    else tail(number * accumulator, number - 1)

  def nor(n: Int): Int = if (n == 1) throw new Exception else n * nor(n - 1)
  def tai(n: Int, acc: Int = 1): Int = if (n == 1) throw new Exception else tai(n -  1,  acc * n)
}
