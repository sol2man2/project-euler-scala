package com.lge.swcec.projecteulerscala.p0035

import com.lge.swcec.projecteulerscala.p0000.ListUtil
import com.lge.swcec.projecteulerscala.p0000.Prime

class CircularPrimes {

  def getTheNumberOfCircularPrimesBelowOneMillion: Int = {
    val list = getCircularPrimesBelowOneMillion
    list.length
  }

  def getCircularPrimesBelowOneMillion: Array[Int] = {
    getCircularPrimesBelowSpecificNumber(1000000)
  }
  
  def getTheNumberOfCircularPrimesBelowSpecificNumber(number: Int): Int = {
    val list = getCircularPrimesBelowSpecificNumber(number)
    list.length
  }

  def getCircularPrimesBelowSpecificNumber(number: Int): Array[Int] = {
    val prime = new Prime
    val primes = prime.makePrimesThruSieveOfEratosThenes(number)
//    val set = prime.primeSet

    val circularPrimes = primes.filter(suspectCircularPrime => {
      val suspectCircularPrimeList = suspectCircularPrime.toString.map(elem => elem.toString.toInt).toList
      val results = for (idx <- 0 to suspectCircularPrimeList.length - 1) yield {

        val suspectPrime = ListUtil.transferListtoInt(suspectCircularPrimeList.drop(idx) ::: suspectCircularPrimeList.take(idx))
        prime.isPrime(suspectPrime)
      }
      results.forall(result => {
        result match {
          case false => false
          case _ => true //result.get == true
        }
      })
    })
    
    circularPrimes
  }
}