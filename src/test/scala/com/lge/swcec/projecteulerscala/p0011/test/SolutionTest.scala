package com.lge.swcec.projecteulerscala.p0011.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0011._

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 11: test doCalc") {
    new Solution().doCalc
  }

  after {
  }
}