package com.lge.swcec.projecteulerscala.p0000

import scala.annotation.tailrec

object ListUtil {

  def transferListtoInt(list: List[Int]): Int = {
    val reverse = list.reverse

    @tailrec
    def makeInt(list: List[Int], sum: Int, digit: Int): Int = {
      list match {
        case head :: tail =>
          makeInt(tail, sum + head * Math.pow(10, digit).toInt, digit + 1)
        case Nil => sum
      }
    }
    makeInt(reverse, 0, 0)
  }
  
  def transferNumberFrom10BasedTo2Based(number: Int): String = {
    def transferNumber(num: Int, twoBased: List[String]): String = {
      val quotient: Int = num / 2
      val remainder: Int = num % 2
      
      quotient match {
        case 0 => (remainder.toString :: twoBased).mkString("")
        case _ => transferNumber(quotient, remainder.toString :: twoBased)
      }
    }
    transferNumber(number, Nil)
  }

}
