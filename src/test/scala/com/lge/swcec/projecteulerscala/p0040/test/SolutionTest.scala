package com.lge.swcec.projecteulerscala.p0040.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0040.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 40: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}