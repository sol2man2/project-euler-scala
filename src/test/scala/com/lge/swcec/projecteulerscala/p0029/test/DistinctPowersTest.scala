package com.lge.swcec.projecteulerscala.p0029.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0029.DistinctPowers

class DistinctPowersTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("The number of (2~5) is 15") {
    val distinctPowers = new DistinctPowers(5, 5)
    val result = distinctPowers.getTheDistinctPowersCases
    assert( 15 == result )
  }

  test("The number of (2~100) is ?") {
    val distinctPowers = new DistinctPowers(100, 100)
    val result = distinctPowers.getTheDistinctPowersCases
    assert( 9183 == result )
  }

  after {
    
  }

}