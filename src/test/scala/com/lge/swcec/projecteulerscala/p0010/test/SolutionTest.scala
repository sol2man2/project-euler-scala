package com.lge.swcec.projecteulerscala.p0010.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0010._

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 10: test doCalc") {
    new Solution().doCalc
  }

  after {
  }
}