package com.lge.swcec.projecteulerscala.p0012.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0003._
import com.lge.swcec.projecteulerscala.p0012._
import scala.collection.mutable.HashMap

class FactorialTest extends FunSuite with BeforeAndAfter {

  var factorial: Factorial = _
  before {
    factorial = new Factorial 
  }

  ignore("Problem 12: test get 25!") {
    val result = factorial.getNFactorial(25)

    assert(result == 2076180480)
  }

  test("Problem 12: test get 5!") {
    val result = factorial.getNFactorial(5)

//    assert(result == 2076180480)
  }

  ignore("Problem 12: test get 25! with tail recursion") {
    val result = factorial.getNFactorialWithTailRecursion(25)

    assert(result == 2076180480)
  }

  test("Problem 12: test get 5! with tail recursion") {
    val result = factorial.getNFactorialWithTailRecursion(5)

//    assert(result == 2076180480)
  }

  after {
  }
}