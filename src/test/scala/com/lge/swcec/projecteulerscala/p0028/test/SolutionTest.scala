package com.lge.swcec.projecteulerscala.p0028.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0028.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 28: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}
