package com.lge.swcec.projecteulerscala.p0034.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0034.DigitFactorials

class DigitFactorialsTest extends FunSuite with BeforeAndAfter {

  var digitFactorials: DigitFactorials = _

  before {
    digitFactorials = new DigitFactorials
  }

  ignore("study list") {
    val words = List("one", "two", "one", "three", "four", "two", "one")
    val grouped = words.groupBy(w => w)
    val counts = grouped.mapValues(_.length)
    
    println("grouped: " + grouped)
    println("counts: " + counts)
  }

  ignore("study factorial") {
    val factorial = DigitFactorials.getFactorial(10)
    println("factorial: " + factorial)

    assert(3628800 == factorial)
  }

  ignore("study int to List[Int]") {
    val list = DigitFactorials.getListInt(4683)
    println("list: " + list)

    assert(list == List(4,6,8,3))
  }

  test("sum of all list of digits factorials are 40730") {
    val result = digitFactorials.AllNumbersWhichAreEqualToTheSumOfTheFactorialOfTheirDigits(10000000)

    assert(result == 40730)
  }

  after {

  }

}
