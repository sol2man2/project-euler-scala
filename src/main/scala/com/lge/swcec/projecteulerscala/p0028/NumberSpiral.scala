package com.lge.swcec.projecteulerscala.p0028

class NumberSpiral(dimension: Int) {

  val TOP = 0
  val LEFT = 0
  val BOTTOM = indexDimension
  val RIGHT = indexDimension

  def center = dimension / 2
  def indexDimension = dimension - 1

  def getTheSumOfDiagonalsInNumberSpiral(): Long = {
    var sum: Long = 0
    
    for( index <- 0 to indexDimension ) {
      sum = sum + getNumberInCellOf(index, index) + getNumberInCellOf(index, indexDimension - index)
    }

    sum - 1
  }

  def getLayerOf(x: Int, y: Int): Int = {
    val layer = Math.min(
        Math.min(Math.abs(x-0), Math.abs(indexDimension - x)),
        Math.min(Math.abs(y-0), Math.abs(indexDimension - y))
    )
    layer
  }

  def getInnerCells(x: Int, y: Int): Long = {
    val layer = getLayerOf(x, y)
    val story = indexDimension - layer * 2 - 1
    story * story
  }

  def getAdjustedPosition(point: Int, layer: Int): Int = point - layer
  
  def getRelativeCoordinate(x: Int, y: Int): (Int, Int) = {
    val layer = getLayerOf(x, y)
    (getAdjustedPosition(x, layer), getAdjustedPosition(y, layer))
  }

  def getNumberInCellOf(x: Int, y: Int): Long = {
    val innerCells = getInnerCells(x, y)
    val layer = getLayerOf(x, y)
    val (transferredX, transferredY) = getRelativeCoordinate(x, y) 

    val top = TOP
    val left = LEFT
    val bottom = BOTTOM - layer * 2
    val right = RIGHT - layer * 2

    val transferredIndexDemension = indexDimension - 2 * layer
    
    if( top != transferredX && right == transferredY ) {
      innerCells + transferredX + top * 0;
    } else if( bottom == transferredX && right != transferredY ) {
      return innerCells + transferredIndexDemension - transferredY + transferredIndexDemension * 1
    } else if( bottom != transferredX && left == transferredY ) {
      return innerCells + transferredIndexDemension - transferredX + transferredIndexDemension * 2
    } else if( top == transferredX && left != transferredY ) {
      return innerCells + transferredY + transferredIndexDemension * 3
    } else {
      return 1
    }
  }
}
