package com.lge.swcec.projecteulerscala.p0000.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0000.ListUtil

class ListUtilTest extends FunSuite with BeforeAndAfter {

  before {
  }

  test("test transfer 10 to ?? in 10 based to 2 based ") {
    val number = 4
    
    val result = ListUtil.transferNumberFrom10BasedTo2Based(number)
    
    println("result: " + result)

    assert(result == "100")
  }

  after {

  }

}

