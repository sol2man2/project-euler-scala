package com.lge.swcec.projecteulerscala.p0016.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0016._

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
  }

  test("Problem 16: test doCalc") {
    new Solution().doCalc
  }

  after {
  }
}
