name := "Project Euler Scala"
 
version := "0.1"
 
scalaVersion := "2.11.0"

resolvers ++= Seq(
  "oss scala-tools"  at "http://oss.sonatype.org/content/groups/scala-tools/",
  "oss releases"  at "http://oss.sonatype.org/content/repositories/releases",
  "oss snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/",
  "mvnrepository" at "http://mvnrepository.com/artifact/",
  Classpaths.typesafeReleases, 
  Classpaths.typesafeSnapshots
//  "typesafe releases" at "http://repo.typesafe.com/typesafe/releases/",
//  "typesafe snapshots" at "http://repo.typesafe.com/typesafe/snapshots/",
)

libraryDependencies ++= Seq(
  "org.specs2" %% "specs2" % "2.3.11" % "test",
//  "org.specs2" %% "specs2-scalaz-core" % "7.0.0" % "test",
  "org.specs2" % "specs2-scalaz-core_2.10" % "7.0.0" % "test",
  "org.scalatest" %% "scalatest" % "2.1.3",
  "org.scalacheck" %% "scalacheck" % "1.11.3",
  "com.novocode" % "junit-interface" % "0.10" % "test",
  "junit" % "junit" % "4.11" % "test",
//  "org.mongodb" %% "casbah" % "2.5.0",
  "org.mongodb" % "casbah_2.10" % "2.7.0",
  "postgresql" % "postgresql" % "9.1-901.jdbc4",
  "net.sf.opencsv" % "opencsv" % "2.1",
  "joda-time" % "joda-time" % "2.3",
  "org.joda" % "joda-convert" % "1.6",
  "log4j" % "log4j" % "1.2.17",
  "com.typesafe.akka" %% "akka-actor" % "2.3.2",
  "com.typesafe.akka" %% "akka-remote" % "2.3.2",
  "com.typesafe.akka" %% "akka-testkit" % "2.3.2",
  "com.typesafe.akka" %% "akka-slf4j" % "2.3.2",
  "com.typesafe.akka" %% "akka-osgi" % "2.3.2",
  "com.typesafe.akka" %% "akka-cluster" % "2.3.2",
  "com.typesafe.akka" %% "akka-kernel" % "2.3.2",
  "com.typesafe.akka" %% "akka-agent" % "2.3.2",
  "com.typesafe.akka" %% "akka-camel" % "2.3.2",
  "io.spray" % "spray-caching" % "1.3.1",
  "io.spray" % "spray-can" % "1.3.1",
  "io.spray" % "spray-client" % "1.3.1",
  "io.spray" % "spray-http" % "1.3.1",
  "io.spray" % "spray-httpx" % "1.3.1",
  "io.spray" % "spray-io" % "1.3.1",
  "io.spray" % "spray-routing" % "1.3.1",
  "io.spray" % "spray-json_2.10" % "1.2.5",
  "org.scalanlp" % "breeze-math_2.10" % "0.4",
  "org.scalanlp" % "breeze-learn_2.10" % "0.3",
  "org.scalanlp" % "breeze-process_2.10" % "0.3",
  "org.scalanlp" % "breeze-viz_2.10" % "0.5.2",
  "org.scala-lang" % "scala-compiler" % "2.11.0",
  "org.scala-lang" % "scala-reflect" % "2.11.0",
  "org.scala-lang" % "scala-actors" % "2.11.0",
//  "org.scala-lang" % "scala-swing" % "2.11.0-M7"
  "org.scala-lang" % "scala-swing" % "2.11.0-M7"
)
