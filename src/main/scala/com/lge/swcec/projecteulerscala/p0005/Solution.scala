package com.lge.swcec.projecteulerscala.p0005

class Solution {

  var multiLcm: MultiLcm = new MultiLcm
  
  def doCalc {
    val result = multiLcm.getMultiLcm((1 to 20))
    println("===============================================================================")
    println("*************************          Problem 5          *************************")
    println("What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?")
    println("Answer: " + result)
    println("*************************          Problem 5          *************************")
    println("===============================================================================")
  }

}