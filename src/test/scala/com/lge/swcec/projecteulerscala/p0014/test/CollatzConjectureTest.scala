package com.lge.swcec.projecteulerscala.p0014.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0014._

class CollatzConjectureTest extends FunSuite with BeforeAndAfter {

  var collatzConjecture: CollatzConjecture = _
  
  before {
    collatzConjecture = new CollatzConjecture
  }

  test("Problem 14: test get after 13's Collatz Conjecture") {
    var next = collatzConjecture.next(13)
    assert(next == 40)
    
    next = collatzConjecture.next(40)
    assert(next == 20)
    
    next = collatzConjecture.next(20)
    assert(next == 10)
    
    next = collatzConjecture.next(10)
    assert(next == 5)
    
    next = collatzConjecture.next(5)
    assert(next == 16)
    
    next = collatzConjecture.next(16)
    assert(next == 8)
    
    next = collatzConjecture.next(8)
    assert(next == 4)

    next = collatzConjecture.next(4)
    assert(next == 2)
    
    next = collatzConjecture.next(2)
    assert(next == 1)
  }

  test("Problem 14: test get after 13 Collatz Conjecture") {
    var count = collatzConjecture.sequenceCountOf(16)
    assert(count == 5)

    count = collatzConjecture.sequenceCountOf(13)
    assert(count == 10)
  }	

  test("Problem 14: test get after 4 Collatz Conjecture seq list") {
    var list = collatzConjecture.calcSequenceCountOf(4)

    assert(list == List(4,2))
  }

  test("Problem 14: test get after 16 Collatz Conjecture seq list") {
    var list = collatzConjecture.calcSequenceCountOf(16)

    assert(list == List(16,8,4,2))
  }

  test("Problem 14: test get after 13 Collatz Conjecture seq list") {
    var list = collatzConjecture.calcSequenceCountOf(13)

    assert(list == List(13,40,20,10,5,16,8,4,2))
  }

  ignore("Problem 14: test get after 113383 Collatz Conjecture seq list") {
    var list = collatzConjecture.calcSequenceCountOf(113383)

    assert(list == List(13,40,20,10,5,16,8,4,2))
  }
  
  test("Problem 14: test") {
    val collatz = collatzConjecture.collatzHavingLongestSequenceWithin(1000000)

    assert( 837799 == collatz )
  }

  after {
  }
}
