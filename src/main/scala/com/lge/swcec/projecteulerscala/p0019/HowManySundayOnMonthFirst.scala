package com.lge.swcec.projecteulerscala.p0019

import org.joda.time.DateTime

class HowManySundayOnMonthFirst {

  def getTheNumberOfSundays = {
    val durationOfYear = (1901 to 2000).toList
    val durationOfMonth = (1 to 12).toList

    val date = new DateTime(1900, 1, 1, 0, 0, 0)
    var firstSundayCount = 0

    for( year <- durationOfYear; month <- durationOfMonth) {
      val modifiedDate = date.withYear(year).withMonthOfYear(month)
      if( modifiedDate.dayOfWeek().getAsText() == "일요일" ) {
        firstSundayCount += 1
      }
    }
    
    println("Total Sunday's Count: " + firstSundayCount)
    firstSundayCount
  }
}