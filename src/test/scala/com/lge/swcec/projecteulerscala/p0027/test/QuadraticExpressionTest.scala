package com.lge.swcec.projecteulerscala.p0027.test

import java.util.concurrent.atomic.AtomicReference
import org.scalatest.BeforeAndAfter
import scala.collection.immutable.Map
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0027.QuadraticExpression

class QuadraticExpressionTest extends FunSuite with BeforeAndAfter {

  before {
  }

  after {

  }

  test("test n**2 + n + 41 is 43, when n is 1") {
    val quadraticExpression = QuadraticExpression.get(1, 41)
    val result = quadraticExpression.evaluate(1)
    val expected = 43

    assert(result == expected)
  }

  test("test n**2 + n + 41 is 43, when n is 4") {
    val quadraticExpression = QuadraticExpression.get(1, 41)
    val result = quadraticExpression.evaluate(4)
    val expected = 61

    assert(result == expected)
  }

  test("test n**2 - 79*n + 1601 is 43, when n is 1") {
    val quadraticExpression = QuadraticExpression.get(-79, 1601)
    val result = quadraticExpression.evaluate(1)
    val expected = 1523

    assert(result == expected)
  }

  test("test n**2 - 79*n + 1601 is 43, when n is 4") {
    val quadraticExpression = QuadraticExpression.get(-79, 1601)
    val result = quadraticExpression.evaluate(4)
    println("result: " + result)
    val expected = 1301

    assert(result == expected)
  }
}
