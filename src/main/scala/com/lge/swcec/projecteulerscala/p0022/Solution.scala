package com.lge.swcec.projecteulerscala.p0022

class Solution {

  val summationOfSortedNamesScores = new SummationOfSortedNamesScores

  def doCalc {
    val sumOfSortedNamesScores = summationOfSortedNamesScores.getSumOfSortedNamesScores("names.txt")

    println("===============================================================================")
    println("*************************          Problem 22         *************************")
    println("What is the total of all the name scores in the file?")
    println("Answer: " + sumOfSortedNamesScores)
    println("*************************          Problem 22         *************************")
    println("===============================================================================")
  }
}