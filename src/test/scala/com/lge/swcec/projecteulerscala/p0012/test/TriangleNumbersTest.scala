package com.lge.swcec.projecteulerscala.p0012.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0003._
import com.lge.swcec.projecteulerscala.p0012._

class TriangleNumbersTest extends FunSuite with BeforeAndAfter {

  var triangleNumbers: TriangleNumbers = _
  
  before {
    triangleNumbers = new TriangleNumbers
  }

  test("Problem 12: test get 1st triangle number") {
    val firstTriangleNumber = triangleNumbers.next
    assert(firstTriangleNumber == 1)
  }

  test("Problem 12: test get 12375th triangle number") {
    val triangleNumber = triangleNumbers.getNthTriangleNumbers(12375)
    println("tri:12375: " + triangleNumber)
    assert(triangleNumber == 76576500)
  }
  
  
  test("Problem 12: test get 7th triangle number") {
    var triangleNumber = 0
    for(index <- 0 until 7) {
      triangleNumber = triangleNumbers.next
    }
    assert(triangleNumber == 28)
//    1,2,4,7,14,28
  }

  test("Problem 12: test get 1000 triangle numbers") {
    var triangleNumber = 0
    for(index <- 0 until 1000) {
      triangleNumber = triangleNumbers.next
    }
    val list = triangleNumbers.getTriangleNumbers
//    println("triangle: " + list)
    
    val primeFactors = new PrimeFactors
    val primeFactorsList = primeFactors.getPrimeFactors(list.last)
    println("factors: " + primeFactorsList + ", size: " + primeFactorsList.size + 2)
    
//    assert(triangleNumber == 28)
  }

 ignore("Problem 12: test get triangle numbers between 1 to 100000") {
    var list = triangleNumbers.getTriangleNumbers(1, 100000)
    println("list between: " + list)
 }

  ignore("Problem 12: test get 1000000th triangle number") {
    var triangleNumber = 0
    for(index <- 0 until 1000000) {
      triangleNumber = triangleNumbers.next
      if(index % 1000 == 0) println("index: " + index / 1000)
    }
    assert(triangleNumber == 28)
  }

  after {
  }
}