package com.lge.swcec.projecteulerscala.p0034

class DigitFactorials {

  def getSumOfAllNumbersWhichAreEqualToTheSumOfTheFactorialOfTheirDigits = {
    AllNumbersWhichAreEqualToTheSumOfTheFactorialOfTheirDigits(10000000)
  }

  def AllNumbersWhichAreEqualToTheSumOfTheFactorialOfTheirDigits(upperLimit: Int): Int = {
    val numbers = (3 to upperLimit).toList

    val filtered = numbers.filter(number =>{
      val list = DigitFactorials.getListInt(number)
      val sumOfDigitFactorial = list.foldLeft(0)((sum, num) => sum + DigitFactorials.getFactorial(num))

      sumOfDigitFactorial == number
    })

    filtered.sum
  }
}

object DigitFactorials {
  import scala.collection.mutable.HashMap
  val map = new HashMap[Int, Int]

  def getListInt(number: Int) = {
    number.toString.toList.map(char => char.toString.toInt)
  }

  def getFactorial(number: Int): Int = {
    number match {
      case 0 => 1
      case 1 => 1
      case 2 => 2
      case _ => {
        if (map.contains(number)) {
          map(number)
        } else {
          val factorial = getFactorial(number - 1) * number
          map(number) = factorial
          factorial
        }
      }
    }
  }
}