package com.lge.swcec.projecteulerscala.p0029.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0029.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 29: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}
