package com.lge.swcec.projecteulerscala.p0025

import com.lge.swcec.projecteulerscala.p0025._

class Solution {

  val fibonacci = new Fibonacci1

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 25         *************************")
    println("What is the first term in the Fibonacci sequence to contain 1000 digits?")

    val thousandDigitFibonacciNumber = fibonacci.getThousandDigitFibonacciNumber()
//    println("fibo: " + thousandDigitFibonacciNumber._1 + ", term: " + thousandDigitFibonacciNumber._2)

    println("Answer: " + thousandDigitFibonacciNumber._2)
    println("*************************          Problem 25         *************************")
    println("===============================================================================")
  }
}
