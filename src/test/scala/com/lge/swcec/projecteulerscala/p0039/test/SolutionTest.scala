package com.lge.swcec.projecteulerscala.p0039.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0039.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 39: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}