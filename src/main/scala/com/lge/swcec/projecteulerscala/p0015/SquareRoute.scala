package com.lge.swcec.projecteulerscala.p0015

import com.lge.swcec.projecteulerscala.p0012
import p0012.Combination

class SquareRoute {

  def count(row: Int, column: Int): Long = {
    
    var countRoute: Long = 0
    var increase: Long = 0
    def routing(ro: Int, co: Int) {

//      increase += 1
//      if( increase % 1000000000 == 0 ) println("inc: " + increase); 
//      println("ro: " + ro + ", co: " + co)

      ro + 1 match {
        case r if(r > row) => 	
        case r if(r == row && co == column) => {
          countRoute += 1
        }
        case _ => routing(ro + 1, co)
      }
      co + 1 match {
        case c if(c > column) => 
        case c if(c == column && ro == row) => {
          countRoute += 1
        }
        case _ => routing(ro, co + 1)
      }
    }
    routing(0, 0)
    countRoute
  }

  def countWithAlgorithm(row: Int, column: Int): Long = {
    val combination = new Combination

    (0 to row).toList.map(
      elem => {
        elem.toLong
      }
    ).foldLeft[Long](0)(
      (sum, elem) => {
        sum +  combination.getNCombinationR(row.toLong, elem.toLong) * combination.getNCombinationR(row.toLong, elem.toLong)
      }
    )
  }
}
