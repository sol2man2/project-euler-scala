package com.lge.swcec.projecteulerscala.p0012

import com.lge.swcec.projecteulerscala.p0003._
import scala.collection.mutable.HashMap
import math._

class FindTriangleNumberHavingNDivisor {

  val triangleNumbers = new TriangleNumbers
  val primeFactors = new PrimeFactors
  val combination = new Combination

  def getFirstTriangleNumberHavingOverNDivisor(upperLimit: Int): Int = {
    def getNextTriangelNumber: Int = {
      val triangleNumber = triangleNumbers.next
      val countDivisors = getTheCountOfDivisorsOfTriangleNumber(triangleNumber)
//      println("[" + triangleNumbers.list.size + "]triangleN: " + triangleNumber + ", countDivisors: " + countDivisors)
//      println("" + triangleNumbers.list.size + "\t" + triangleNumber + "\t" + countDivisors)
      countDivisors match {
        case count if(count < upperLimit) => getNextTriangelNumber
        case _ => triangleNumber
      }
    }
    getNextTriangelNumber
  }
  
  def getTheCountOfDivisorsOfNthIndexTriangleNumber(nth: Int): Long = {
    val triangleNumber = triangleNumbers.getNthTriangleNumbers(nth)
    getTheCountOfDivisorsOfTriangleNumber(triangleNumber)
  }

  def getTheCountOfDivisorsOfTriangleNumber(number: Int): Long = {
    number match {
      case 1 => 1
      case _ => getTheCountOfDivisors(getCountOfCountMap(getFactorsCountMap(getPrimeFactors(number))))
    }
  }

  def getTheCountOfDivisors(map: HashMap[Int, Int]): Long = {
    var sum: Long = 1

//    map.foreach(
//      pair => {
//        sum *= ( pair._1 + 1 ) * pair._2
//      }
//    )

    map.foreach(
      pair => {
        pair._1 match {
          case 1 => sum *= combination.getSumOfNCombination(pair._2)
//          case _ => sum *= ( pair._1 + 1 ) * pair._2
          case _ => sum *= pow(pair._1 + 1, pair._2).toInt
        }
      }
    )
    sum
  }

  def getCountOfCountMap(map: HashMap[Int, Int]): HashMap[Int, Int] = {
    var factorMap: HashMap[Int, Int] = new HashMap[Int, Int]

    map.foreach(
      pair => {
        factorMap.put(pair._2, factorMap.getOrElse(pair._2, 0) + 1)
      }
    )
    factorMap
  }

  def getFactorsCountMap(list: List[Int]): HashMap[Int, Int] = {
    var map: HashMap[Int, Int] = new HashMap[Int, Int]

    list.foreach(
      elem => {
        map.put(elem, map.getOrElse(elem, 0) + 1)
      }
    )
    map
  }

  def getPrimeFactors(number: Int): List[Int] = {
    primeFactors.getPrimeFactors(number).map(elem => elem.intValue())
  }

  def getNthTriangleNumber(nth: Int): Int = {
    val size = triangleNumbers.list.size
    val gap = nth - size

    for(index <- 0 until gap) {
      triangleNumbers.next
    }
    triangleNumbers.list.last
  }
}
