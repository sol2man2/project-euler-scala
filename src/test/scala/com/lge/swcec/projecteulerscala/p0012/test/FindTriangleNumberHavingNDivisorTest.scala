package com.lge.swcec.projecteulerscala.p0012.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0003._
import com.lge.swcec.projecteulerscala.p0012._
import scala.collection.mutable.HashMap

class FindTriangleNumberHavingNDivisorTest extends FunSuite with BeforeAndAfter {

  var triangleNumber: FindTriangleNumberHavingNDivisor = _
  
  before {
    triangleNumber = new FindTriangleNumberHavingNDivisor
  }

  test("Problem 12: test get nth triangle number") {
    val result = triangleNumber.getNthTriangleNumber(7)
    assert(result == 28)
  }

  test("Problem 12: test get triangle number 7th's divisors") {
    val list = triangleNumber.getPrimeFactors(28)
    assert(list == List(2, 2, 7))
  }

  test("Problem 12: test get triangle number nth's divisor map") {
    val list = triangleNumber.getPrimeFactors(28)
    val map: HashMap[Int, Int]  = triangleNumber.getFactorsCountMap(list)

    assert(map == Map(7->1, 2->2))
  }

  test("Problem 12: test get triangle number nth's divisor factorMap") {
    val list = triangleNumber.getPrimeFactors(28)
    val map: HashMap[Int, Int]  = triangleNumber.getFactorsCountMap(list)
    val result = triangleNumber.getCountOfCountMap(map)

    assert(result == Map(1->1, 2->1))
  }

  test("Problem 12: test get the number of divisor of 1 triangle number") {
    val numDivisors = triangleNumber.getTheCountOfDivisorsOfTriangleNumber(1)

    assert(numDivisors == 1)
  }

  test("Problem 12: test get the number of divisor of 6 triangle number") {
    val numDivisors = triangleNumber.getTheCountOfDivisorsOfTriangleNumber(6)

    assert(numDivisors == 4)
  }

  test("Problem 12: test get the number of divisor of triangle number") {
    val numDivisors = triangleNumber.getTheCountOfDivisorsOfTriangleNumber(28)

    assert(numDivisors == 6)
  }

  test("Problem 12: test get the number of divisor of 7th triangle number ") {
    val numDivisors = triangleNumber.getTheCountOfDivisorsOfNthIndexTriangleNumber(7)

    assert(numDivisors == 6)
  }

  ignore("Problem 12: test get the number of divisor of 76576500 triangle number") {
    val l = triangleNumber.getPrimeFactors(76576500)
    println("l: " + l)
    val m = triangleNumber.getFactorsCountMap(l)
    println("m: " + m)
    val map = triangleNumber.getCountOfCountMap(m)
    println("map: " + map)
    val numDivisors = triangleNumber.getTheCountOfDivisors(map)

    println("76576500: " + numDivisors)

    assert(numDivisors == 576)
  }

  test("Problem 12: test get the number of divisor of 12375th triangle number ") {
    val numDivisors = triangleNumber.getTheCountOfDivisorsOfNthIndexTriangleNumber(12375)

    assert(numDivisors == 576)
  }

  test("Problem 12: test get triangle number having 5 divisors") {
    val result = triangleNumber.getFirstTriangleNumberHavingOverNDivisor(5)

    assert(result == 28)
  }

  test("Problem 12: test get triangle number having 20 divisors") {
    val result = triangleNumber.getFirstTriangleNumberHavingOverNDivisor(20)

    assert(result == 528)
  }

  test("Problem 12: test get triangle number having over 500 divisors") {
    val result = triangleNumber.getFirstTriangleNumberHavingOverNDivisor(500)
    println("triangle number having over 500 divisor: " + result)
    assert(result == 76576500)
  }

  after {
  }
}