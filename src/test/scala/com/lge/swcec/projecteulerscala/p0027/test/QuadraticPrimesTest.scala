package com.lge.swcec.projecteulerscala.p0027.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0027
import com.lge.swcec.projecteulerscala.p0027.QuadraticExpression
import com.lge.swcec.projecteulerscala.p0027.QuadraticPrimes

class QuadraticPrimesTest extends FunSuite with BeforeAndAfter {

  var quadraticPrimes: QuadraticPrimes = null

  before {
    quadraticPrimes = new QuadraticPrimes
  }

  after {

  }

  ignore("test trial of lazy iterator") {
    def log[A](a: A) = { println(a); a }

    println("normal print")
    for (i <- 1 to 10) yield log(i)
    println("normal print end")

    println("lazy print")
    for (i <- (1 to 10) view) yield log(i)
    println("lazy print end")
  }

  ignore("test trial of multi lazy iterator") {
    def doIt(i: Int, j: Int, k: Int) = i + j + k
    val x =
      (1 to 2).map(i => {
        (1 to 2).map(j => {
          (1 to 2).iterator.map(k => doIt(i, j, k))
        }).reduceLeft(_ ++ _)
      }).reduceLeft(_ ++ _)

    x.foreach(println(_))
  }

//  test("test isPrimeUntil") {
//    quadraticPrimes.isPrimeUntil()
//  }

  ignore("test n*n + n + 41 has 40 primes between 0 and 39") {
    val alpha = 1
    val beta = 41

    val expression = QuadraticExpression.get(alpha, beta)
    val value = quadraticPrimes.getLastIndexOnPrimeFrom(expression, 0)
    println("alpha: " + alpha + ", beta: " + beta + ", value: " + (value - 1))

    assert(false)
  }

  ignore("test n*n - 79n + 1601 has 80 primes between 0 and 79") {
    val alpha = -79
    val beta = 1601

    val expression = QuadraticExpression.get(alpha, beta)
    val value = quadraticPrimes.getLastIndexOnPrimeFrom(expression, 0)
    println("alpha: " + alpha + ", beta: " + beta + ", value: " + (value - 1))

    assert(false)
  }

  ignore("test n*n - 999n + 61 has 1011 primes between 0 and 1010") {
    val alpha = -999
    val beta = 61

    val expression = QuadraticExpression.get(alpha, beta)
    val value = quadraticPrimes.getLastIndexOnPrimeFrom(expression, 0)
    println("alpha: " + alpha + ", beta: " + beta + ", value: " + (value - 1))

    assert(false)
  }
  
  ignore("test efficient 10, 10's max prime index") {
    val (maxAlpha, maxBeta, maxIndexOfPrime) = quadraticPrimes.findProductOfCoefficientsWithMaxinumPrime(100,100)
    println("maxAlpha: " + maxAlpha + ", maxBeta: " + maxBeta + ", maxIndexOfPrime: " + maxIndexOfPrime)
    assert(false)
  }

  test("test efficient 1000, 1000's max prime index") {
    val (maxAlpha, maxBeta, maxIndexOfPrime) = quadraticPrimes.findProductOfCoefficientsWithMaxinumPrime(1000,1000)
    println("maxAlpha: " + maxAlpha + ", maxBeta: " + maxBeta + ", maxIndexOfPrime: " + maxIndexOfPrime)
    
    assert(-59231 == maxAlpha * maxBeta)
  }
  
  ignore("test all of (n**2 + n + 41) are primes, when n is in range 0~39") {

    //    quadraticPrimes.get
    //    quadraticPrimes.getTheNumberOfPrime(1, 41, )
    //    val quadraticExpression = QuadraticExpression.get(1, 41)
    //
    //    assert(List.range(0, 40, 1).map(quadraticExpression.evaluate).map(_.toLong).forall(prime.isPrime) == true)
    assert(true)
  }

  ignore("test all of (n**2 + n + 41) are not primes, when n is in range 0~40") {
    val quadraticExpression = QuadraticExpression.get(1, 41)

    //    assert(List.range(0, 41, 1).map(quadraticExpression.evaluate).map(_.toLong).forall(prime.isPrime) == false)
  }
}
