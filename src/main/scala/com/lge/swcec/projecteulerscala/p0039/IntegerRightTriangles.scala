package com.lge.swcec.projecteulerscala.p0039

import com.lge.swcec.projecteulerscala.p0000.ListUtil
import scala.collection.mutable.ListBuffer
import scala.util.control.Breaks._

class IntegerRightTriangles {

  def getTheNumberOfSolutionsMaximisedForWhichValueOfPLessThen1000: Int =
    getTheNumberOfSolutionsMaximisedForWhichValueOfPLessThen(1000)

  def getTheNumberOfSolutionsMaximisedForWhichValueOfPLessThen(maxP: Int): Int = {

    val limit = maxP / 2

    val tryRightAngleTrialgle = for {
      a <- 1 to limit
      b <- 1 to limit
      c <- 1 to limit if a + b + c <= maxP
    } yield {
      if (math.pow(a, 2) + math.pow(b, 2) == math.pow(c, 2)
        || math.pow(a, 2) == math.pow(b, 2) + math.pow(c, 2)
        || math.pow(a, 2) + math.pow(c, 2) == math.pow(b, 2)) List(a, b, c).sorted
      else
        Nil
    }
    val rightAngleTrialgle = tryRightAngleTrialgle.filter(_ != Nil).groupBy(elem => elem).keys.toList
    val mapPToLength = rightAngleTrialgle.groupBy(elem => elem(0) + elem(1) + elem(2)).mapValues(elem => elem.length)
    val listPAndLength = mapPToLength.toList
    val (ps, lengths) = listPAndLength.unzip
    val maxLength = lengths.max
    val pValue = ps(lengths.indexOf(maxLength))
    pValue
  }

}