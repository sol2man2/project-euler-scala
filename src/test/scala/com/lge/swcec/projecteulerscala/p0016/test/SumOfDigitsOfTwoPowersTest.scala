package com.lge.swcec.projecteulerscala.p0016.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0016._

class SumOfDigitsOfTwoPowersTest extends FunSuite with BeforeAndAfter {

  var sumOfDigitsOfTwoPowers: SumOfDigitsOfTwoPowers = _
  
  before {
    sumOfDigitsOfTwoPowers = new SumOfDigitsOfTwoPowers
  }

  test("Problem 16: test get 2^15") {
    val number = sumOfDigitsOfTwoPowers.getTwoPower(15)

    assert(number == 32768)
  }

  test("Problem 16: test get 2^1000") {
    val number = sumOfDigitsOfTwoPowers.getTwoPower(1000)

    assert(number == BigInt("107150860718626732094842504906000181056140481170553360744375038837035105" +
      "11249361224931983788156958581275946729175531468251871452856923140435984577574698" +
      "57480393456777482423098542107460506237114187795418215304647498358194126739876755" +
      "9165543946077062914571196477686542167660429831652624386837205668069376"))
  }

  test("Problem 16: test get the sum of 2^15's digits") {
    val number = sumOfDigitsOfTwoPowers.getSumOfDigitsOfTwoPowers(15)

    assert(number == 26)
  }

  test("Problem 16: test get the sum of 2^1000's digits") {
    val number = sumOfDigitsOfTwoPowers.getSumOfDigitsOfTwoPowers(1000)

    assert(number == 1366)
  }

  after {
  }
}