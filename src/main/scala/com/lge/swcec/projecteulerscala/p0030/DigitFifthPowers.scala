package com.lge.swcec.projecteulerscala.p0030

import scala.collection.mutable.HashSet

class DigitFifthPowers {

  def getTheSumOfMatchedOfDigitFifthPowers: Int = {

    getDigitFifthPowersList
  }
  
  
  def getDigitFifthPowersList: Int = {
    var set = new HashSet[Int]
    var idx = 0
    val list = (2 to 360000 ).filter( isDigitNthPowers(_, 5) )
    list.sum
  }

  def getDigitFourthPowersList: Int = {
    var set = new HashSet[Int]
    val list = (2 to 10000 ).filter( isDigitNthPowers(_, 4) )
    list.sum
  }

  def isDigitNthPowers(elem: Int, nth: Int): Boolean = {
    val list = getDigitListFromNumeric(elem)
    val result = sumOfDigitNthPowers(list, nth)

    elem == result
  }
  
  def sumOfDigitNthPowers(list: List[Int], nth: Int): Int = {
    def getTheElemOfThePowerOfFourth(sum: Int, elem: Int): Int = {
      Math.pow(elem, nth).toInt + sum
    }
    list.foldLeft(0)(getTheElemOfThePowerOfFourth(_, _))
  }
  
  def getDigitListFromNumeric(number: Int): List[Int] = {
    number.toString.toList.map( _.toString ).map( _.toInt )
  }

}
