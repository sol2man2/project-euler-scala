package com.lge.swcec.projecteulerscala.p0038.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0038.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 38: test doCalc") {
    new Solution().doCalc
  }

  after {
    
  }

}