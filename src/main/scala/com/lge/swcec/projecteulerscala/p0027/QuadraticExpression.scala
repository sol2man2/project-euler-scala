package com.lge.swcec.projecteulerscala.p0027

class QuadraticExpression(alpha: Int, beta: Int) {
  def evaluate(variable: Long): Long = {
    variable * variable + alpha * variable + beta
  }

 
  override def toString(): String = {
    val sb: StringBuilder = new StringBuilder
    sb.append("var*var").append(" ").append(alpha).append("var ").append(beta).toString
  }
}

object QuadraticExpression {
  def get(alpha: Int, beta: Int): QuadraticExpression = {
    new QuadraticExpression(alpha, beta)
  }
}
