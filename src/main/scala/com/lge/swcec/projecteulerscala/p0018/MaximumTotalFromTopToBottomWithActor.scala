package com.lge.swcec.projecteulerscala.p0018

class MaximumTotalFromTopToBottomWithActor {

  def getMainMaximumTotal(): Int = {
    getMaximumTotal(TriangleData.listsMainTriangle)
  }
  
  def getSampleMaximumTotal(): Int = {
    getMaximumTotal(TriangleData.listsSampleTriangle)
  }
  
  var total = 0

  def getMaximumTotal(lists: List[List[Int]]): Int = {
    findMaximumTotal(0, 0, lists)
    total
  }
  
  def findMaximumTotal(index: Int, sum: Int, lists: List[List[Int]]): Unit = {
    lists match {
      case Nil => {
        if( total < sum ) total = sum
      }
      case headList :: tailLists => {
        findMaximumTotal( index, sum + headList(index), tailLists )
        findMaximumTotal( index + 1, headList(index) + sum, tailLists )
      } 
    }
  }

}