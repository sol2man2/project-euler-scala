package com.lge.swcec.projecteulerscala.p0002

import scala.annotation.tailrec

class FibonacciStream {

  val getNth: Stream[Int] = 1 #:: getNth.scanLeft(2)(_ + _)
  
  def getSumOfFibonacciListBelow4000000: Int = {
    getFibonacciListBelow4000000.sum
  }

  def getFibonacciListBelow4000000: List[Int] = {
    getFibonacciListBelow(4000000)
  }

  def getFibonacciListBelow(upperLimit: Int): List[Int] = {
    @tailrec
    def extractFibonaccci(index: Int): Int = {
      getNth(index) match {
        case fibo if(fibo <= upperLimit) => extractFibonaccci(index + 1)
        case fibo if(fibo > upperLimit) => index
      }
    }
    getNth.take(extractFibonaccci(0)).toList
  }
}
