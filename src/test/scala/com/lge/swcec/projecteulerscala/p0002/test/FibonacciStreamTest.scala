package com.lge.swcec.projecteulerscala.p0002.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0002._

class FibonacciStreamTest extends FunSuite with BeforeAndAfter {

  var fibonacciStream: FibonacciStream = _

  before {
    fibonacciStream = new FibonacciStream
  }

  test("") {
    val results = fibonacciStream.getFibonacciListBelow(4000000)

    assert(results.last == 4613732)
  }

  after {
  }

}