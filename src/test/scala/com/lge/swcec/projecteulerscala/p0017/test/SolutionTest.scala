package com.lge.swcec.projecteulerscala.p0017.test;

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0017._

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
  }

  test("Problem 17: test get the sum of 2^1000's digits") {
    new Solution().doCalc
  }

  after {
  }
}
