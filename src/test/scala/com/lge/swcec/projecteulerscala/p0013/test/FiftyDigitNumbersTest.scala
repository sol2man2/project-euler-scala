package com.lge.swcec.projecteulerscala.p0013.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0013._

class FiftyDigitNumbersTest extends FunSuite with BeforeAndAfter {

  var fiftyDigitNumbers: FiftyDigitNumbers = _
  
  before {
    fiftyDigitNumbers = new FiftyDigitNumbers
  }

  test("Problem 13: test get sum of one hundred of fifty digit number") {
    val sum = fiftyDigitNumbers.sum
    println("50 digit sum: " + sum)
    assert(sum == BigInt("5537376230390876637302048746832985971773659831892672"))
    //	5537376230
  }

//  test("Problem 13: test get ??") {
//    val target = 137373600 
//    val list = (1 to (target + 1)/2).toList//.filter( elem => target % elem == 0)
//    println("list size: " + list.size)
//  }

  after {
  }
}
