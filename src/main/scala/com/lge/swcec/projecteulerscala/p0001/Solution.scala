package com.lge.swcec.projecteulerscala.p0001

class Solution {
  def doCalc() {
    val solution = new ThreeOrFiveList
    val sum = solution.getSumOfAllTheMultiplesOf3Or5Below1000
    println("===============================================================================")
    println("*************************          Problem 1          *************************")
    println("Find the sum of all the multiples of 3 or 5 below 1000.")
    println("Answer: " + sum)
    println("*************************          Problem 1          *************************")
    println("===============================================================================")
  }
}
