package com.lge.swcec.projecteulerscala.p0030

class Solution {

  val digitFifthPowers = new DigitFifthPowers

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 30         *************************")
    println("Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.")

    val theSumOfMatchedBetweenTheSumOfDigitFifthPowersAndSelf = digitFifthPowers.getTheSumOfMatchedOfDigitFifthPowers

    println("Answer: " + theSumOfMatchedBetweenTheSumOfDigitFifthPowersAndSelf)
    println("*************************          Problem 30         *************************")
    println("===============================================================================")
  }

}