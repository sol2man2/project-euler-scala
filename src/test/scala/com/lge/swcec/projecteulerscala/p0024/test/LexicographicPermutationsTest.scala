package com.lge.swcec.projecteulerscala.p0024.test

import scala.collection.Seq
import org.scalatest.FunSuite
import scala.collection.immutable.Map
import scala.reflect.Manifest
import scala.runtime.BoxedUnit
import java.lang.reflect.Method
import org.scalatest.BeforeAndAfter
import com.lge.swcec.projecteulerscala.p0024._
import scala.collection.mutable.ListBuffer

class LexicographicPermutationsTest extends FunSuite with BeforeAndAfter {

  var lexicographicPermutations: LexicographicPermutations = _

  before {
    lexicographicPermutations = new LexicographicPermutations("0123456789")
  }

  ignore("test ListBuffer sorted") {
    val lb = ListBuffer(3,6,2)
    val sortedLb = lb.sorted

    println("lb: " + sortedLb)
  }

  test("test cases are when n fixed") {
    assert(362880 == lexicographicPermutations.getTheNumberOfCasesInFixed(0))
    assert(40320 == lexicographicPermutations.getTheNumberOfCasesInFixed(1))
    assert(5040 == lexicographicPermutations.getTheNumberOfCasesInFixed(2))
    assert(720 == lexicographicPermutations.getTheNumberOfCasesInFixed(3))
    assert(120 == lexicographicPermutations.getTheNumberOfCasesInFixed(4))
    assert(24 == lexicographicPermutations.getTheNumberOfCasesInFixed(5))
    assert(6 == lexicographicPermutations.getTheNumberOfCasesInFixed(6))
    assert(2 == lexicographicPermutations.getTheNumberOfCasesInFixed(7))
    assert(1 == lexicographicPermutations.getTheNumberOfCasesInFixed(8))
  }

  test("test 362881th's index list is List(1,0,0,0,0,0,0,0,0,0)") {
    val countList = lexicographicPermutations.getCountListOfEachDigit(362881)

    assert( countList == List(1,0,0,0,0,0,0,0,0,0) )
  }

  test("test 1000000th's index list is List(2,6,6,2,5,1,2,1,1,0)") {
    val countList = lexicographicPermutations.getCountListOfEachDigit(1000000)

    println("1000000's list: " + countList)
    
    assert( countList == List(2,6,6,2,5,1,2,1,1,0) )
  }

  test("test first lexicographic permutations is 0123456789") {
    val firstLexicographicPermutation = lexicographicPermutations.getNthLexicographicPermutation(1)

//    println("firstLexicographicPermutation: " + firstLexicographicPermutation.toString)

    assert( List('0','1','2','3','4','5','6','7','8','9') == firstLexicographicPermutation )
  }

  test("test 362881th's lexicographic permutations is 1023456798") {
    val countList = lexicographicPermutations.getNthLexicographicPermutation(362881)
    println("countList: " + countList)

    assert( List('1','0','2','3','4','5','6','7','8','9') == countList )
  }

  test("test 1000000th's lexicographic permutations is ???") {
    val countList = lexicographicPermutations.getNthLexicographicPermutation(1000000)

    println("1000000's lexcicographic permutation: " + countList)
    
    assert( countList == List('2','7','8','3','9','1','5','4','6','0') )
  }

  ignore("test ListBuffer's remove") {
    var listBuffer = ListBuffer(1,2,3)
    val removed = listBuffer.remove(1)
    println("removed: " + removed)
    println("listBuffer: " + listBuffer)
  }
  
  after {
    
  }
}
