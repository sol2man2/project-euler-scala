package com.lge.swcec.projecteulerscala.p0031.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0030.DigitFifthPowers
import com.lge.swcec.projecteulerscala.p0031.CoinSums

class CoinSumsTest extends FunSuite with BeforeAndAfter {

  before {
  }

  test("cases to make 2L is 73682") {
    val coinSums = new CoinSums
    
    val result = coinSums.getTheCaseOfTheWayToMakeL2

//    println("cases: " + result)
    
    assert( 73682 == result )
  }

  after {
    
  }

}