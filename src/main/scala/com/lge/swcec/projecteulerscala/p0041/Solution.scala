package com.lge.swcec.projecteulerscala.p0041

class Solution {

  val pandigitalPrime = new PandigitalPrime

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 41         *************************")
    println("What is the largest n-digit pandigital prime that exists?")

    val theLargestNdigitPandigitalPrimeThatExists = pandigitalPrime.getTheLargestNdigitPandigitalPrimeThatExists

    println("Answer: " + theLargestNdigitPandigitalPrimeThatExists)
    println("*************************          Problem 41         *************************")
    println("===============================================================================")
  }

}