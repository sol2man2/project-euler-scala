package com.lge.swcec.projecteulerscala.p0009.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0009._

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
    
  }

  test("Problem 9: test doCalc") {
    new Solution().doCalc
  }

  after {
  }
}