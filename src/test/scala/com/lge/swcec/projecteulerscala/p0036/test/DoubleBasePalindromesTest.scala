package com.lge.swcec.projecteulerscala.p0036.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0000.ListUtil
import com.lge.swcec.projecteulerscala.p0036.DoubleBasePalindromes

class DoubleBasePalindromesTest extends FunSuite with BeforeAndAfter {

  var doubleBasePalindromes: DoubleBasePalindromes = _

  before {
    doubleBasePalindromes = new DoubleBasePalindromes
  }

  test("the sum of all numbers less than one million with palindromic both 10 base and 2 base") {

    val result = doubleBasePalindromes.getTheSumOfAllNumbersLessThanOneMillionWithPalindromicBothBase10AndBase2
    
    assert(result == 872187)
  }

  after {

  }

}

