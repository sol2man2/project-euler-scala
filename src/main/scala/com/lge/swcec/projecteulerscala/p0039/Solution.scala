package com.lge.swcec.projecteulerscala.p0039

class Solution {

  val integerRightTriangles = new IntegerRightTriangles

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 39         *************************")
    println("For which value of p ≤ 1000, is the number of solutions maximised?")

    val theNumberOfSolutionsMaximisedForWhichValueOfPLessThen1000 = integerRightTriangles.getTheNumberOfSolutionsMaximisedForWhichValueOfPLessThen1000

    println("Answer: " + theNumberOfSolutionsMaximisedForWhichValueOfPLessThen1000)
    println("*************************          Problem 39         *************************")
    println("===============================================================================")
  }

}