package com.lge.swcec.projecteulerscala.p0006.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0006._

class SumAndSquareTest extends FunSuite with BeforeAndAfter {

  var sumAndSquare: SumAndSquare = _

  before {  
    sumAndSquare = new SumAndSquare
  }

  test("Problem 6: test SquareAfterSum vs. SumAfterSquare 1 to 10") {
    val result = sumAndSquare.getDeltaEachOther(1, 10)

    assert(result == 2640)
  }

  test("Problem 6: test SquareAfterSum vs. SumAfterSquare 1 to 100") {
    val result = sumAndSquare.getDeltaEachOther(1, 100)

    println("sum square: " + result)
    assert(result == 25164150)
  }

  after {
  }
}