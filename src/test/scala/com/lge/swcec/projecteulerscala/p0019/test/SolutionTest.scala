package com.lge.swcec.projecteulerscala.p0019.test

import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import com.lge.swcec.projecteulerscala.p0019.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
  }

  test("Problem 19: test doCalc") {
    new Solution().doCalc
  }

  after {
  }
}
