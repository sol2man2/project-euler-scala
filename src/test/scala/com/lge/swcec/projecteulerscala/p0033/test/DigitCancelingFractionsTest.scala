package com.lge.swcec.projecteulerscala.p0033.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0033.DigitCancelingFractions

class DigitCancelingFractionsTest extends FunSuite with BeforeAndAfter {

  var digitCancelingFractions: DigitCancelingFractions = _

  before {
    digitCancelingFractions = new DigitCancelingFractions
  }

  ignore("get digit of 22 is List(2,2)") {

    val result = digitCancelingFractions.getDigits(22)
    println("result: " + result)

    assert(result == List(2, 2))
  }

  ignore("get GCD") {
    println("(3,4)" + digitCancelingFractions.getGCD(3, 4))
    println("(12,8)" + digitCancelingFractions.getGCD(12, 8))
  }
  
  ignore("list diff") {

    val list1 = List(1, 2)
    val list2 = List(2, 3)

    val result1 = list1.diff(list2)
    println("result1: " + result1)

    assert(result1 == List(1))

    val list3 = List(6, 6)
    val list4 = List(5, 6)

    val result2 = list3.diff(list4)
    println("result2: " + result2)

    assert(result2 == List(6))

    val list5 = List(5, 6)
    val list6 = List(6, 6)

    val result3 = list5.diff(list6)
    println("result3: " + result3)

    assert(result3 == List(6))

  }

  ignore("get denominator of 4 fractions") {
    val result = digitCancelingFractions.getDigitCancelingFractions
    println("result: " + result)
  }

  test("denominator of 4 fractions' product is 100") {
    val result = digitCancelingFractions.getDenominatorOfFourFractionsProduct
    
    println("result: " + result)

    assert(result == 100)
  }

  after {

  }

}
