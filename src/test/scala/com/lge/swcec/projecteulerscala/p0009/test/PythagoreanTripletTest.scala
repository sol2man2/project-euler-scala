package com.lge.swcec.projecteulerscala.p0009.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0009._

class PythagoreanTripletTest extends FunSuite with BeforeAndAfter {

  var pythagoreanTriplet: PythagoreanTriplet = _
  before {
    pythagoreanTriplet = new PythagoreanTriplet 
  }

  test("Problem 9: test natural square root") {
    val result = pythagoreanTriplet.isSquareRootNaturalNumber(2)
    assert(result == false)
  }

  test("Problem 9: test square root") {
    var result = pythagoreanTriplet.getSquareRoot(4)
    assert(result == 2)
    
    result = pythagoreanTriplet.getSquareRootNaturalNumber(4)
    assert(result == 2)

    result = pythagoreanTriplet.getSquareRootNaturalNumber(2)
    assert(result == -1)

    result = pythagoreanTriplet.getSquareRoot(2)
    assert(result == 1.4142135623730951)
  }

  test("Problem 9: test pow") {
    val result = Math.pow(4,2)
    assert(result == 16.0)
  }

  test("Problem 9: test find equation as Pythagorean triplet") {
    val result = pythagoreanTriplet.getProductOfEquationAsPythagoreanFactorsSum(1000)
    println("py solution: " + result)
    assert(result == 31875000)
  }

  test("") {
    val result = pythagoreanTriplet.getTheProductAbc(1000)

    assert(result == 31875000)
  }

  ignore("Problem 9: test combinations") {
    val r = 1 to 3
    val l = r.toList
    val li = l.combinations(2)
    
    println("class of li: " + li.getClass())
    println("li: " + li)
    li.foreach(
      elem => {
        println(elem)
      }
    )
  }

  after {
  }
}
