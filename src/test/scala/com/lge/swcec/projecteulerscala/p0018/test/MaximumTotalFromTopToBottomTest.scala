package com.lge.swcec.projecteulerscala.p0018.test

import com.lge.swcec.projecteulerscala.p0018.MaximumTotalFromTopToBottom
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.sun.org.apache.xalan.internal.xsltc.compiler.ForEach
import com.lge.swcec.projecteulerscala.p0018.TriangleData

class MaximumTotalFromTopToBottomTest extends FunSuite with BeforeAndAfter {
  var maximumTotalFromTopToBottom: MaximumTotalFromTopToBottom = _

  before {
    maximumTotalFromTopToBottom = new MaximumTotalFromTopToBottom
  }

  test("Problem 18: test sample's maximum total") {
    val lists = TriangleData.listsSampleTriangle
    val maximumTotal = maximumTotalFromTopToBottom.getMaximumTotal(lists)

    assert(maximumTotal == 23)
  }

  test("Problem 18: test main's maximum total") {
    val lists = TriangleData.listsMainTriangle
    val maximumTotal = maximumTotalFromTopToBottom.getMaximumTotal(lists)

    assert(maximumTotal == 1074)
  }

  after {
  }

}
