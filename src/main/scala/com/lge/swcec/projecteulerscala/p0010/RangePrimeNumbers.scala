package com.lge.swcec.projecteulerscala.p0010

import com.lge.swcec.projecteulerscala.p0007.IndexingPrimeNumbers
import shapeless.ToList

class RangePrimeNumbers extends IndexingPrimeNumbers {

  def getTheSumSfAllThePrimesBelowTwoMillion: Long = {
    makePrimesThruSieveOfEratosThenes(2000000)
    var sum = 0
    
    val list = for {
      index <- 0 until primes.length
    } yield {
      primes(index)
    }
    
    list.foldLeft[Long](0)(_ + _)
  }
}