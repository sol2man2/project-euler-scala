package com.lge.swcec.projecteulerscala.p0005

class MultiLcm {

  val gcmLcm: GcmLcm = new GcmLcm

  def getMultiLcm(range: Range): Long = {
    
    val list = range.toList
    list.foldLeft(1L)((init, elem) => gcmLcm.getLcm(init, elem))
  }
}
