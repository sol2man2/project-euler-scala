package com.lge.swcec.projecteulerscala.p0020

class Solution {

  val summationOfFactorialDigit = new SummationOfFactorialDigit

  def doCalc {
    val sumOfFactorialDigit = summationOfFactorialDigit.getSumOfFactorialDigit(100)

    println("===============================================================================")
    println("*************************          Problem 20         *************************")
    println("Find the sum of the digits in the number 100!")
    println("Answer: " + sumOfFactorialDigit)
    println("*************************          Problem 20         *************************")
    println("===============================================================================")
  }

}