package com.lge.swcec.projecteulerscala.p0034

class Solution {

  val digitFactorials = new DigitFactorials

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 34         *************************")
    println("Find the sum of all numbers which are equal to the sum of the factorial of their digits.")

    
    val theSumOfAllNumbersWhichAreEqualToTheSumOfTheFactorialOfTheirDigits = digitFactorials.getSumOfAllNumbersWhichAreEqualToTheSumOfTheFactorialOfTheirDigits

    println("Answer: " + theSumOfAllNumbersWhichAreEqualToTheSumOfTheFactorialOfTheirDigits)
    println("*************************          Problem 34         *************************")
    println("===============================================================================")
  }

}