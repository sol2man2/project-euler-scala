package com.lge.swcec.projecteulerscala.p0012

class Solution {

  var triangleNumber = new FindTriangleNumberHavingNDivisor

  def doCalc {
    val result = triangleNumber.getFirstTriangleNumberHavingOverNDivisor(500)

    println("===============================================================================")
    println("*************************          Problem 12         *************************")
    println("What is the value of the first triangle number to have over five hundred divisors?")
    println("Answer: " + result)
    println("*************************          Problem 12         *************************")
    println("===============================================================================")
  }
}