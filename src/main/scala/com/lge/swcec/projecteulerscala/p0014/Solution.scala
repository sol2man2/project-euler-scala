package com.lge.swcec.projecteulerscala.p0014

class Solution {

  var collatzConjecture = new CollatzConjecture

  def doCalc {
  val collatz = collatzConjecture.collatzHavingLongestSequenceWithin(1000000)

    println("===============================================================================")
    println("*************************          Problem 14         *************************")
    println("Which starting number, under one million, produces the longest chain?")
    println("Answer: " + collatz)
    println("*************************          Problem 14         *************************")
    println("===============================================================================")
  }
}