package com.lge.swcec.projecteulerscala.p0028.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0028.NumberSpiral

class NumberSpiralTest extends FunSuite with BeforeAndAfter {

  var numberSpiral: NumberSpiral = null

  before {
  }

  test("test (0,0) has 0 layer cells on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val layer = numberSpiral.getLayerOf(0, 0)
    assert( layer == 0 )
  }

  test("test (4,4) has 0 layer cells on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val layer = numberSpiral.getLayerOf(4, 4)
    assert( layer == 0 )
  }

  test("test (1,1) has 1 layer cells on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val layer = numberSpiral.getLayerOf(1, 1)
    assert( layer == 1 )
  }

  test("test (3,3) has 1 layer cells on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val layer = numberSpiral.getLayerOf(3, 3)
    assert( layer == 1 )
  }

  test("test (1,3) has 1 layer cells on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val layer = numberSpiral.getLayerOf(1, 3)
    assert( layer == 1 )
  }

  test("test (3,1) has 1 layer cells on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val layer = numberSpiral.getLayerOf(3, 1)
    assert( layer == 1 )
  }

  test("test (0,1) has 1 layer cells on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val layer = numberSpiral.getLayerOf(0, 1)
    assert( layer == 0 )
  }

  test("test (0,1) has 9 inner cells on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val theNumberOfInnerCells = numberSpiral.getInnerCells(0, 1)
    assert( theNumberOfInnerCells == 9 )
  }

  test("test (1,2) has 1 inner cell on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val theNumberOfInnerCells = numberSpiral.getInnerCells(1, 2)
    assert( theNumberOfInnerCells == 1 )
  }

  test("test (1,2) transfer (0,1) on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val (x, y) = numberSpiral.getRelativeCoordinate(1, 2)
    assert( x == 0 && y == 1 )
  }

  test("test (1,1) transfer (0,0) on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val (x, y) = numberSpiral.getRelativeCoordinate(1, 1)
    assert( x == 0 && y == 0 )
  }

  test("test (2,3) transfer (1,2) on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val (x, y) = numberSpiral.getRelativeCoordinate(2, 3)
    assert( x == 1 && y == 2 )
  }

  test("test (3,3) transfer (2,2) on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val (x, y) = numberSpiral.getRelativeCoordinate(3, 3)
    assert( x == 2 && y == 2 )
  }

  test("test (4,4) transfer (4,4) on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val (x, y) = numberSpiral.getRelativeCoordinate(4, 4)
    assert( x == 4 && y == 4 )
  }

  test("test (2,2) transfer (0,0) on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val (x, y) = numberSpiral.getRelativeCoordinate(2, 2)
    assert( x == 0 && y == 0 )
  }

  test("test (0,0) transfer (0,0) on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val (x, y) = numberSpiral.getRelativeCoordinate(2, 2)
    assert( x == 0 && y == 0 )
  }

  test("test value in (1,1) is 10 on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val number = numberSpiral.getNumberInCellOf(1, 1)

    assert( 7 == number )
  }

  test("test value in (2,1) is 10 on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val number = numberSpiral.getNumberInCellOf(2, 1)
    
    println("???" + number)
    
    assert( 6 == number )
  }

  test("test value in (2,3) is 10 on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val number = numberSpiral.getNumberInCellOf(2, 3)

    assert( 2 == number )
  }

  test("test value in (1,4) is 10 on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val number = numberSpiral.getNumberInCellOf(1, 4)
    assert( 10 == number )
  }

  test("test value in (4,4) is 10 on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val number = numberSpiral.getNumberInCellOf(4, 4)
    assert( 13 == number )
  }

  test("test value in (4,0) is 10 on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val number = numberSpiral.getNumberInCellOf(4, 0)
    assert( 17 == number )
  }

  test("test value in (0,0) is 10 on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val number = numberSpiral.getNumberInCellOf(0, 0)
    assert( 21 == number )
  }

  test("test value in (0,4) is 10 on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val number = numberSpiral.getNumberInCellOf(0, 4)
    assert( 25 == number )
  }

  test("test value in (3,3) is 10 on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val number = numberSpiral.getNumberInCellOf(3, 3)
    assert( 3 == number )
  }

  test("test the sum of the number spiral diagonals is 101 on 5*5") {
    val dim = 5
    numberSpiral = new NumberSpiral(dim)
    val number = numberSpiral.getTheSumOfDiagonalsInNumberSpiral
    assert( 101 == number )
  }

  test("test the sum of the number spiral diagonals is 101 on 1001*1001") {
    val dim = 1001
    numberSpiral = new NumberSpiral(dim)
    val number = numberSpiral.getTheSumOfDiagonalsInNumberSpiral
    assert( 669171001 == number )
  }

  after {
    
  }

}