package com.lge.swcec.projecteulerscala.p0007.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0007._

class SolutionTest extends FunSuite with BeforeAndAfter {

	before {
	}

	test("Problem 7: test doCalc") {
		new Solution().doCalc
	}

	after {
	}
}