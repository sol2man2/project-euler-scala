package com.lge.swcec.projecteulerscala.p0031

class Solution {

  val coinSums = new CoinSums

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 31         *************************")
    println("How many different ways can £2 be made using any number of coins?")

    val theCaseOfTheWayToMakeL2 = coinSums.getTheCaseOfTheWayToMakeL2

    println("Answer: " + theCaseOfTheWayToMakeL2)
    println("*************************          Problem 31         *************************")
    println("===============================================================================")
  }

}