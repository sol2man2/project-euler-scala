package com.lge.swcec.projecteulerscala.p0006

class Solution {

  var sumAndSquare = new SumAndSquare
  
  def doCalc {
    val result = sumAndSquare.getDeltaEachOther(1, 100)
    println("===============================================================================")
    println("*************************          Problem 6          *************************")
    println("Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.")
    println("Answer: " + result)
    println("*************************          Problem 6          *************************")
    println("===============================================================================")
  }
}