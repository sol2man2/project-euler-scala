package com.lge.swcec.projecteulerscala.p0033

class DigitCancelingFractions {

  def getDenominatorOfFourFractionsProduct = {
    val tuples = getDigitCancelingFractions
    val product = tuples.reduce( (a, b) => (a._1 * b._1, a._2 * b._2) )
    val gcd = getGCD(product._1, product._2)
    product._2 / gcd
  }

  def getGCD(first:Int, second: Int): Int = {
    first % second match {
      case 0 => second
      case remains@_ => getGCD(second, remains)
    }
  }
  
  def getDigitCancelingFractions: List[(Int, Int)] = {

    val result = for {
      numerator <- 10 to 99
      denominator <- 10 to 99 if (numerator != denominator && !(numerator % 10 == 0 && denominator % 10 == 0))
    } yield {
      val fraction = numerator.toFloat / denominator.toFloat

      val numeratorList = getDigits(numerator)
      val denominatorList = getDigits(denominator)

      val diffNumer = numeratorList.diff(denominatorList)
      val diffDenom = denominatorList.diff(numeratorList)

      if (diffNumer.length == 1 && diffDenom.length == 1 && diffDenom.head != 0
          && diffNumer.head.toFloat / diffDenom.head.toFloat == fraction
          && diffNumer.head.toFloat / diffDenom.head.toFloat < 1)
          List(numerator, denominator, diffNumer.head, diffDenom.head)
      else Nil
    }

    result.toList.filter(_ != Nil).map(list => (list(2), list(3)))
  }

  def getDigits(number: Int): List[Int] = {
    val tenDigit = number / 10
    val oneDigit = number - tenDigit * 10
    List(tenDigit, oneDigit)
  }

}