package com.lge.swcec.projecteulerscala.p0032.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0032.PandigitalProducts
import scala.collection.mutable.ListBuffer

class PandigitalProductsTest extends FunSuite with BeforeAndAfter {

  var pandigitalProducts: PandigitalProducts = _

  before {
    pandigitalProducts = new PandigitalProducts
  }

  test("sum of 9 Pnadigital Produccts is 45228") {

    val result = pandigitalProducts.getCasesOfNinePandigitalProducts
    println("result: " + result)
    println("result: " + result.sum)

    assert(45228 == result.groupBy(elem => elem).keySet.sum)
  }

  ignore("cases of 9 Pnadigital Produccts is ???") {
    val pandigitalProducts = new PandigitalProducts

    val result = pandigitalProducts.getCasesOfNinePandigitalProducts
  }

  ignore("get subset of List(1,2,3)") {
    val factors = List(1, 2, 3)
    val subsets = pandigitalProducts.getSubset(factors)

    println("subset: " + subsets)

    assert(subsets == List(
      List(1),
      List(2, 1), List(2),
      List(3, 1), List(3, 2, 1), List(3, 2), List(3)))
  }

  ignore("get subset of List(1,2,3,4)") {
    val factors = List(1, 2, 3, 4)
    val subsets = pandigitalProducts.getSubset(factors)

    assert(subsets == List(
      List(1),
      List(2, 1), List(2),
      List(3, 1), List(3, 2, 1), List(3, 2), List(3),
      List(4, 1), List(4, 2, 1), List(4, 2), List(4, 3, 1), List(4, 3, 2, 1), List(4, 3, 2), List(4, 3), List(4)))
  }

  ignore("get subset of List(1,2,3,4,5)") {
    val factors = List(1, 2, 3, 4, 5)
    val subsets = pandigitalProducts.getSubset(factors).filter(subset => {
      val validDigitLength = factors.length % 2 match {
        case 1 => factors.length / 2 + 1
        case 0 => factors.length / 2
      }
      validDigitLength > subset.length
    })

    println("suspects: " + subsets)
    println("suspects size: " + subsets.length)

    assert(true)
  }

  ignore("get subset of List(1,2,3,4,5,6,7,8,9)") {
    val factors = List(1, 2, 3, 4, 5, 6, 7, 8, 9)
    val subsets = pandigitalProducts.getSubset(factors).filter(subset => {
      val validDigitLength = factors.length % 2 match {
        case 1 => factors.length / 2 + 1
        case 0 => factors.length / 2
      }
      validDigitLength > subset.length
    })

    println("suspect size: " + subsets.length)

    assert(true)
  }

  ignore("ListBuffer method") {
    val lb = ListBuffer(1, 2)
    val permu = for (idx <- 0 to lb.length) yield {
      val tmp = lb.clone
      println("tmp class: " + tmp.getClass)
      tmp.insert(idx, 7)
      tmp
    }
    val permuList = permu.toList
    //    lb.insertAll(1, List(5,7))
    println("lb: " + lb)
    println("permu: " + permu)
    println("permuList: " + permuList)
  }

  ignore("int calc") {
    val a = 9
    val b = a / 2

    println("b: " + b)
    assert(true)
  }

  ignore("for yield") {
    val pair = for (x <- 1 to 2; y <- 1 to 2) yield {
      if (x == y) List(x, y)
      else Nil
    }

    println("pair: " + pair.toList.filter(_ != Nil))

    assert(false)
  }

  ignore("int list") {
    val list = List(1, 2, 3)

    def TransferListtoInt(list: List[Int]) = {
      val reverse = list.reverse

      def makeInt(list: List[Int], sum: Int, digit: Int): Int = {
        list match {
          case head :: tail =>
            println("head: " + head + ",sum: " + sum + ", digit: " + digit)
            makeInt(tail, sum + head * Math.pow(10, digit).toInt, digit + 1)
          case Nil => sum
        }
      }
      makeInt(reverse, 0, 0)
    }
    val result = TransferListtoInt(list)
    println("result: " + result)

  }

  ignore("get permutation of List(1,2)") {
    val factors = List(1, 2)
    val permutations = pandigitalProducts.getPermutation(factors)
    println("permutations: " + permutations)

    assert(permutations == List(List(2, 1), List(1, 2)))
  }

  ignore("get permutation of List(1,2,3)") {
    val factors = List(1, 2, 3)
    val permutations = pandigitalProducts.getPermutation(factors)
    println("permutations: " + permutations)

    assert(permutations == List(
      List(3, 2, 1), List(2, 3, 1), List(2, 1, 3),
      List(3, 1, 2), List(1, 3, 2), List(1, 2, 3)))
  }

  after {

  }

}
