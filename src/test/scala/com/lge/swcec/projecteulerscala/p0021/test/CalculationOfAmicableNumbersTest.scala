package com.lge.swcec.projecteulerscala.p0021.test

import scala.collection.Seq
import org.scalatest.FunSuite
import scala.collection.immutable.Map
import scala.reflect.Manifest
import scala.runtime.BoxedUnit
import java.lang.reflect.Method
import org.scalatest.BeforeAndAfter
import com.lge.swcec.projecteulerscala.p0021.CalculationOfAmicableNumbers
import com.lge.swcec.projecteulerscala.p0003.PrimeFactors
import com.lge.swcec.projecteulerscala.p0021.ProperDivisors

class CalculationOfAmicableNumbersTest extends FunSuite with BeforeAndAfter {

  var calculationOfAmicableNumbers: CalculationOfAmicableNumbers = _
  var properDivisors: ProperDivisors = _

  before {
    calculationOfAmicableNumbers = new CalculationOfAmicableNumbers
    properDivisors = new ProperDivisors
  }

//  prime factorization
  ignore("test - prime factorization") {
    val primeFactors = new PrimeFactors
    val primes = primeFactors.getPrimeFactors(21)
  }

  ignore("test - d(6)") {
    val list = properDivisors.getProperDivisors(6)
    val d= properDivisors.d(6)
    println("list: " + list)
    println("6-? :" + d)
//    println("huj: " + num)
  }

  ignore("test - d(28)") {
    val list = properDivisors.getProperDivisors(28)
    val d = properDivisors.d(28)
    println("28-list: " + list)
    println("28-? :" + d)
//    println("huj: " + num)
  }

  ignore("test - d(8128)") {
    val list = properDivisors.getProperDivisors(8128)
    val d = properDivisors.d(8128)
    println("8128-list: " + list)
    println("8128-? :" + d)
//    println("huj: " + num)
  }

  ignore("test - prime") {
    val list = new ProperDivisors().getProperDivisors(220)
    assert(List(1, 2, 4, 5, 10, 11, 20, 22, 44, 55, 110) == list)
//    println("huj: " + num)
  }

  ignore("test whether 6 is amicable number with ??") {
    val number = 6
    val (flag, amicableNumber, amicablePair) = calculationOfAmicableNumbers.getAmicableNumbers(number)

    println(" :" + amicableNumber + ", " + amicablePair)
    
    assert(flag && amicablePair == 284)
  }

  ignore("test whether 220 is amicable number with 284") {
    val number = 220
    val (flag, amicableNumber, amicablePair) = calculationOfAmicableNumbers.getAmicableNumbers(number)

    assert(flag && amicablePair == 284)
  }

  test("test whether sum of amicable numbers under 10000 is ???") {
    val number = 10000
    val sum = calculationOfAmicableNumbers.getSumOfAmicableNumbers(number)

    println("sum of 10000's amicable number is " + sum)
    assert(31626 == sum)
  }

  ignore("test - prime factors") {
    val primeFactors = new PrimeFactors
    val primes = primeFactors.getPrimeFactors(21)
    println("primes: " + primes)
    assert(true)
  }

  after {
    
  }
}
