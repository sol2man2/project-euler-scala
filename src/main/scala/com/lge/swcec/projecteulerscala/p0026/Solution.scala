package com.lge.swcec.projecteulerscala.p0026

import com.lge.swcec.projecteulerscala.p0026._


class Solution {

  val recoprocalCycles = new ReciprocalCycles
  val digit = 2000

  def doCalc {
    println("===============================================================================")
    println("*************************          Problem 26         *************************")
    println("Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.")

    val theNumberOflongestReciprocalCyclesUnder2000 = recoprocalCycles.getTheNumberOfLongestRecurringCycleUnder1000(digit)

    println("Answer: " + theNumberOflongestReciprocalCyclesUnder2000)
    println("*************************          Problem 26         *************************")
    println("===============================================================================")
  }
}
