package com.lge.swcec.projecteulerscala.p0008

class Solution {

  var thousandDigit = new ThousandDigit

  def doCalc {
    val max = thousandDigit.getMax
    println("===============================================================================")
    println("*************************          Problem 8          *************************")
    println("Find the greatest product of five consecutive digits in the 1000-digit number.")
    println("Answer: " + max)
    println("*************************          Problem 8          *************************")
    println("===============================================================================")
  }
}