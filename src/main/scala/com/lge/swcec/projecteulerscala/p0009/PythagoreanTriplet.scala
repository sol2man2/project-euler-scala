package com.lge.swcec.projecteulerscala.p0009

class PythagoreanTriplet {

  def isPythagoreanTriplet(first: Double, second: Double, third: Double): Boolean = {
    first * first + second * second == third * third
  } 
  
  def isSquareRootNaturalNumber(value: Double): Boolean = {
    val squreRoot = Math.sqrt(value)
    squreRoot == Math.floor(squreRoot)
  }

  def getSquareRootNaturalNumber(value: Double): Int = {
    val result = isSquareRootNaturalNumber(value)
    result match {
      case true => Math.floor(Math.sqrt(value)).toInt
      case false => -1
    }
  }

  def getSquareRoot(value: Double): Double = {
    Math.sqrt(value)
  }

  def findEquationAsPythagoreanFactorsSum(mustSum: Int): Tuple3[Int, Int, Int] = {
    val rangeFirst = 1 to mustSum
    val listFirst = rangeFirst.toList

    var list: List[Double] = Nil

    listFirst.foreach(
      elemFirst => {
        val rangeSecond = 1 to (mustSum - elemFirst)
        val listSecond = rangeSecond.toList
        
        listSecond.foreach(
            elemSecond => {
              val third = getSquareRootNaturalNumber(Math.pow(elemFirst, 2) + Math.pow(elemSecond, 2))
              third match {
                case -1 =>
                case _ => {
                  val sum: Int = elemFirst + elemSecond + third
                  if(sum == mustSum) return (elemFirst, elemSecond, third)
                }
              }
            }
        )
      }
    )

    (0, 0, 0)
  }

  def getProductOfEquationAsPythagoreanFactorsSum(mustSum: Int): Int = {
    val tuple = findEquationAsPythagoreanFactorsSum(mustSum)
    tuple._1 * tuple._2 * tuple._3
  }
  
  def getTheProductAbc(summation: Int): Int = {
    val lists = for {
      a <- 1 to summation - 1
      b <- 1 to summation - a
      c = summation - a - b
      if( a * a + b * b == c * c)
    } yield {
      (a, b, c)
    }
    println("lists size: " + lists.length)
    
    val productList = lists.map(tuple => {
      tuple._1 * tuple._2 * tuple._3
    })
    
    productList.max
  }
}