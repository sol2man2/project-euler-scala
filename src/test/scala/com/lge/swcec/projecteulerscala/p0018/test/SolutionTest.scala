package com.lge.swcec.projecteulerscala.p0018.test

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import com.lge.swcec.projecteulerscala.p0018.Solution

class SolutionTest extends FunSuite with BeforeAndAfter {

  before {
  }

  test("Problem 18: test doCalc") {
    new Solution().doCalc
  }

  after {
  }
}
