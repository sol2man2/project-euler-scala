package com.lge.swcec.projecteulerscala.p0023

import com.lge.swcec.projecteulerscala.p0021.ProperDivisors
import scala.collection.mutable.ListBuffer

class PerfectNumber extends ProperDivisors {

  def getSumOfTwoNumbersInList(numbers: List[Int], limit: Int): ListBuffer[Int] = {
    numbers.to[ListBuffer].map( _ + numbers.head ).filter( _ < limit )
  }

//  @tailrec
  def getSumOfTwoNumbersRecursive(abundantNumbers: List[Int], upperLimit: Int, result: ListBuffer[Int]): ListBuffer[Int] = {
    abundantNumbers match {
      case head::tail => {
        getSumOfTwoNumbersRecursive(abundantNumbers.tail, upperLimit, result ++= getSumOfTwoNumbersInList(abundantNumbers, upperLimit).distinct)
      }
      case Nil => result
    }
  }

  def getSumOfNonAbundantNumbers(upperLimit: Int): Int = {
    val abundantNumbers = getAbundantNumber(upperLimit)
    val nonAbundantNumberArray = getNonAbundantNumbers(abundantNumbers, upperLimit)

    var sum: Int = 0
    for( index <- 0 until upperLimit) {
      if(nonAbundantNumberArray(index) == false) sum += index + 1 
    }

    sum
  }

  def getNonAbundantNumbers(abundantNumbers: List[Int], upperLimit: Int): Array[Boolean] = {

    def getSumOfTwoNumbersInList(number: Int, flag: Array[Boolean]) {
      val adjudtUpperLimit = upperLimit - number
      val droppedAbundantNumbers = abundantNumbers.dropWhile( _ < number).reverse.dropWhile( _ > adjudtUpperLimit ).reverse
      val list = droppedAbundantNumbers.map( _ + number )

      for(elem <- list) {
        flag(elem - 1) = true
      }
    }

    var array = Array.ofDim[Boolean](upperLimit)

    for( number <- abundantNumbers ) {
      getSumOfTwoNumbersInList(number, array)
    }

    array
  }

  def getSumOfTwoNumbers(abundantNumbers: List[Int], upperLimit: Int): List[Int] = {

    def getSumOfTwoNumbersInList(number: Int): List[Int] = {
      val adjudtUpperLimit = upperLimit - number
      val droppedAbundantNumbers = abundantNumbers.dropWhile( _ < number).reverse.dropWhile( _ > adjudtUpperLimit ).reverse
      droppedAbundantNumbers.map( _ + number )
    }

    var lb = new ListBuffer[Int]

    for( number <- abundantNumbers ) {
      println("number: " + number)
      lb ++= getSumOfTwoNumbersInList(number)
      lb.distinct
    }

    lb.distinct.to[List]
  }

  def getPerfectNumber(number: Int): List[Int] = {
    val list = List.range(1, number, 1)

    list.filter(isPerfectNumber)
  }

  def getDeficientNumber(number: Int): List[Int] = {
    val list = List.range(1, number, 1)

    list.filter(isDeficientNumber)
  }

  def getAbundantNumber(number: Int): List[Int] = {
    val list = List.range(1, number, 1)

    list.filter(isAbundantNumber)
  }
  
  def isPerfectNumber(number: Int): Boolean = {
    val resultD = d(number)
    (number == resultD)
  }

  def isDeficientNumber(number: Int): Boolean = {
    val resultD = d(number)
    (number > resultD)
  }
  
  def isAbundantNumber(number: Int): Boolean = {
    val resultD = d(number)
    (number < resultD)
  }

}