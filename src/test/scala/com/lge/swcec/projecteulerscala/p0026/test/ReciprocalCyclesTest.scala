package com.lge.swcec.projecteulerscala.p0026.test

import scala.reflect.Manifest

import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite

import com.lge.swcec.projecteulerscala.p0026.ReciprocalCycles

class ReciprocalCyclesTest extends FunSuite with BeforeAndAfter {

  var reciprocalCycles: ReciprocalCycles = _
  val limit = 1000
  val digit = 1000
  val testLimit = 10
  val str1by6 = "16666666666666666"
  val str1by7 = "14285714285714285"
  val str1by73 = "02941176470588235294117647058823529"

  before {
    reciprocalCycles = new ReciprocalCycles
  }

  ignore("test List[Char] -> String") {
    val list: List[Char] = List('1','4','2','8','5','7')

    val str = list.mkString
    val re = 1.0 / 6.0
    
    println("1/6: " + re)
    
    val range = List.range(1, 10, 1)
    
    println("range: " + range)
  }

  ignore("test Is empty String -> List Nil?") {
    val list = "".to[List]
    println("list: " + list)
  }

//  
//  
  ignore("test string compare") {
    val compare = "142857"
    val to = "142857"
    assert(compare == to)

    val list: List[Char] = List('1','4','2','8','5','7')
    val arr = list.toArray
    val str = String.valueOf(arr)

    println( "arr: " + arr + ", str: " + str)

  }

  ignore("test big decimal") {
    val bigDecimal = BigDecimal(1) / BigDecimal(73)
    println("bigDecimal 1/73: " + bigDecimal)

    val limit = 20
    val oneToLimit = List.range(1, limit).to[List]
    val onepointoneToLimit = oneToLimit.map( BigDecimal(1) / BigDecimal(_) )

    println("onepoint: " + onepointoneToLimit)
    
  }


//  
//  
  ignore("test split list") {
    val list = List('1','4','2','8','5','7','1','4','2','8','5','7','1','4','2','8','5')

    val lists = list.mkString.grouped(6)
    println("split list: " + lists)
    for( elem <- lists ) {
      println(": " + elem.getClass)
    }

    assert(lists == List(List('1','4','2','8','5','7'), List('1','4','2','8','5','7'), List('1','4','2','8','5')))
  }
  
//  
//  
  ignore("test the longest reciprocal cycle under 1000 is ?") {
    val longest = reciprocalCycles.getTheNumberOfLongestRecurringCycleUnder1000(digit*10)

    println("longest: " + longest)
    
    assert( 73 == longest )
  }

  ignore("test the longest reciprocal cycle under 1000 is ??") {
    val longest = reciprocalCycles.getTheNumberOfLongestRecurringCycle(limit, digit * 2)

    println("longest: " + longest)
    
    assert( 73 == longest )
  }

  ignore("test the longest reciprocal cycle under 10 is 7") {
    val longest = reciprocalCycles.getTheNumberOfLongestRecurringCycle(testLimit, digit)

    println("longest: " + longest)

    assert( 7 == longest )
  }

//  
//  
  ignore("test getReciprocalNumber 1/2~1/1000") {
    val list = List.range(3, 1001, 2)
    val set = list.map(reciprocalCycles.getReciprocalNumber(_, 2000))
    
    println("list size: " + set.length)

  }

  ignore("test getReciprocalNumber 1/2") {
    val decimal = reciprocalCycles.getReciprocalNumber(2, 100)
    println("decimal(1/2): " + decimal)
  }
  
  ignore("test getReciprocalNumber 1/3") {
    val decimal = reciprocalCycles.getReciprocalNumber(3, 100)
    println("decimal(1/3): " + decimal)
  }

  ignore("test getReciprocalNumber 1/7") {
    val decimal = reciprocalCycles.getReciprocalNumber(7, 100)
    println("decimal(1/7): " + decimal)
  }

  ignore("test getReciprocalNumber 1/952") {
    val decimal = reciprocalCycles.getReciprocalNumber(952, 100)
    println("decimal(1/952): " + decimal)
  }

  ignore("test getReciprocalNumber 1/998") {
    val decimal = reciprocalCycles.getReciprocalNumber(998, 1000)
    println("decimal(1/998): " + decimal)
  }

//  new

  ignore("test getReciprocalNumber 1/983") {
    val decimal = reciprocalCycles.getReciprocalNumber(983, 2000)
    println("decimal(1/983): " + decimal)
  }
  
  ignore("test getReciprocalNumber 1/7 another") {
    val decimal = reciprocalCycles.getReciprocalNumber(7, 2000)
    println("decimal(1/7): " + decimal)
  }

  test("test the longest reciprocal cycle under 1000 is ???") {
    val longest = reciprocalCycles.getTheNumberOfLongestRecurringCycleUnder1000(digit*2)

    println("longest: " + longest)
    
    assert( 73 == longest )
  }

  ignore("test the longest reciprocal cycle under 10 is ???") {
    val longest = reciprocalCycles.getTheNumberOfLongestRecurringCycle(10, digit*2)

    println("longest: " + longest)
    
    assert( 73 == longest )
  }
  
  after {
    
  }
}
