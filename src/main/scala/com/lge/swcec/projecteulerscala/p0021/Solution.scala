package com.lge.swcec.projecteulerscala.p0021

class Solution {

  val calculationOfAmicableNumbers = new CalculationOfAmicableNumbers

  def doCalc {
    val sumOfAmicableNumbers = calculationOfAmicableNumbers.getSumOfAmicableNumbers(1000)

    println("===============================================================================")
    println("*************************          Problem 21         *************************")
    println("Evaluate the sum of all the amicable numbers under 10000.")
    println("Answer: " + sumOfAmicableNumbers)
    println("*************************          Problem 21         *************************")
    println("===============================================================================")
  }

}